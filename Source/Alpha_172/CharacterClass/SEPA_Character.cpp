// Copyright Epic Games, Inc. All Rights Reserved.

#include "Alpha_172/CharacterClass/SEPA_Character.h"
#include "Alpha_172/WeaponClass/ProjectileClass/SEPA_ProjectileBase.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
//#include "Alpha_172/ComponentsActor/SEPA_HealthComponent.h"
//#include "Alpha_172/ComponentsActor/SEPA_InventoryComponent.h"
#include "Alpha_172/GameClass/SEPA_GameInstance.h"
//#include "Alpha_172/ComponentsActor/SEPA_ObjectStabilityComponent.h"
#include "Net/UnrealNetwork.h"

//#include "MotionControllerComponent.h"
//#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ASEPA_Character

ASEPA_Character::ASEPA_Character()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	//BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	//FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetupAttachment(GetMesh(), FName("head"));
	FirstPersonCameraComponent->SetRelativeLocation(FVector(8.f, 15.f, 0.f));		// Position the camera
	FirstPersonCameraComponent->SetRelativeRotation(FRotator(90.f, 0.f, 00.f));		// Rotation the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	

	//// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	//Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	//Mesh1P->SetOnlyOwnerSee(true);
	//Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	//Mesh1P->bCastDynamicShadow = false;
	//Mesh1P->CastShadow = false;
	//Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	//Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	//// Create a gun mesh component
	//FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	//FP_Gun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	//FP_Gun->bCastDynamicShadow = false;
	//FP_Gun->CastShadow = false;
	//// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	//FP_Gun->SetupAttachment(RootComponent);

	//FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	//FP_MuzzleLocation->SetupAttachment(FP_Gun);
	//FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	//// Default offset from the character location for projectiles to spawn
	//GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	//// Create VR Controllers.
	//R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	//R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	//R_MotionController->SetupAttachment(RootComponent);
	//L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	//L_MotionController->SetupAttachment(RootComponent);

	//// Create a gun and attach it to the right-hand VR controller.
	//// Create a gun mesh component
	//VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	//VR_Gun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	//VR_Gun->bCastDynamicShadow = false;
	//VR_Gun->CastShadow = false;
	//VR_Gun->SetupAttachment(R_MotionController);
	//VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	//VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	//VR_MuzzleLocation->SetupAttachment(VR_Gun);
	//VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	//VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;


	HealthComponent		= CreateDefaultSubobject<USEPA_HealthComponent>("HealthComponent");
	InventoryComponent	= CreateDefaultSubobject<USEPA_InventoryComponent>("InventoryComponent");
	StaminaComponent	= CreateDefaultSubobject<USEPA_StaminaComponent>("StaminaComponent");
	ProtectionComponent = CreateDefaultSubobject<USEPA_ProtectionComponent>("ProtectionComponent");
	//WeaponComponent = CreateDefaultSubobject<USEPA_WeaponComponent>("WeaponComponent");

	if (HealthComponent)
	{
		HealthComponent->OnHealthLevelChange.AddDynamic(this, &ASEPA_Character::ChangeCurrentValues_ByHealthLevel);
		//HealthComponent->OnHealthChange.AddDynamic(this, &ASEPA_Character::ChangeCurrentValues_ByHealth);
		HealthComponent->OnDead.AddDynamic(this, &ASEPA_Character::CharacterDead);
	}
	//if (InventoryComponent)
	//{
	//	InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ASEPA_Character::InitWeapon);
	//}
	//if (StaminaComponent)
	//{
	//	StaminaComponent->OnStaminaChange.AddDynamic(this, &ASEPA_Character::ChangeStamina);
	//}
	//if (ProtectionComponent)
	//{
	//	ProtectionComponent->OnProtectionLevelChange.AddDynamic(this, &ASEPA_Character::ChangeCurrentValues_ByHealthLevel);
	//}
}

void ASEPA_Character::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	////Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	//FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	//// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	//if (bUsingMotionControllers)
	//{
	//	//VR_Gun->SetHiddenInGame(false, true);
	//	Mesh1P->SetHiddenInGame(true, true);
	//}
	//else
	//{
	//	//VR_Gun->SetHiddenInGame(true, true);
	//	Mesh1P->SetHiddenInGame(false, true);
	//}


	//InitWeapon(EWeaponType::Rifle_Type);
	InitWeapon();
	ChangeCurrentValues_ByHealthLevel();
}

//////////////////////////////////////////////////////////////////////////
// Input

void ASEPA_Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind weapon events
	PlayerInputComponent->BindAction("Fire",					IE_Pressed,	this, &ASEPA_Character::FirePress);					//LEFT MOUSE
	PlayerInputComponent->BindAction("Fire",					IE_Released,this, &ASEPA_Character::FireRelease);				//LEFT MOUSE
	PlayerInputComponent->BindAction("Aim",						IE_Pressed,	this, &ASEPA_Character::AimPress);					//RIGHT MOUSE
	PlayerInputComponent->BindAction("Aim",						IE_Released,this, &ASEPA_Character::AimRelease);				//RIGHT MOUSE
	PlayerInputComponent->BindAction("Reload",					IE_Pressed,	this, &ASEPA_Character::ReloadPress);				//R
	PlayerInputComponent->BindAction("SwitchNexWeapon",			IE_Pressed,	this, &ASEPA_Character::SwitchNextWeaponPress);		//SWITCH MOUSE
	PlayerInputComponent->BindAction("SwitchPreviosWeapon",		IE_Pressed,	this, &ASEPA_Character::SwitchPreviosWeaponPress);	//SWITCH MOUSE
	PlayerInputComponent->BindAction("PickUpDrop_CurrentWeapon",IE_Pressed, this, &ASEPA_Character::PickUpWeaponPress);			//T 2 sec.
	PlayerInputComponent->BindAction("PickUpDrop_CurrentWeapon",IE_Released, this, &ASEPA_Character::PickUpWeaponRelease);		

	PlayerInputComponent->BindAction("SwitchWeaponKit1",		IE_Pressed, this, &ASEPA_Character::SwitchWeaponKit1);			//1
	PlayerInputComponent->BindAction("SwitchWeaponKit2",		IE_Pressed, this, &ASEPA_Character::SwitchWeaponKit2);			//2
	PlayerInputComponent->BindAction("SwitchWeaponKit3",		IE_Pressed, this, &ASEPA_Character::SwitchWeaponKit3);			//3
	PlayerInputComponent->BindAction("SwitchWeaponKit4",		IE_Pressed, this, &ASEPA_Character::SwitchWeaponKit4);			//4
	PlayerInputComponent->BindAction("SwitchWeaponKit5",		IE_Pressed, this, &ASEPA_Character::SwitchWeaponKit5);			//5
	PlayerInputComponent->BindAction("SwitchWeaponKit6",		IE_Pressed, this, &ASEPA_Character::SwitchWeaponKit6);			//6
	PlayerInputComponent->BindAction("SwitchWeaponKit7",		IE_Pressed, this, &ASEPA_Character::SwitchWeaponKit7);			//7
	PlayerInputComponent->BindAction("SwitchWeaponKit8",		IE_Pressed, this, &ASEPA_Character::SwitchWeaponKit8);			//8
	PlayerInputComponent->BindAction("SwitchWeaponKit9",		IE_Pressed, this, &ASEPA_Character::SwitchWeaponKit9);			//9


	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward",			this, &ASEPA_Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight",				this, &ASEPA_Character::MoveRight);
	PlayerInputComponent->BindAxis("Turn",					this, &APawn::AddControllerYawInput);
	//PlayerInputComponent->BindAxis("TurnRate",		this, &ASEPA_Character::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp",				this, &APawn::AddControllerPitchInput);
	//PlayerInputComponent->BindAxis("LookUpRate",	this, &ASEPA_Character::LookUpAtRate);

	PlayerInputComponent->BindAction("OnlyProne", IE_Pressed, this, &ASEPA_Character::OnlyPronePress);		//Z
	PlayerInputComponent->BindAction("OnlyWalk",	IE_Pressed, this, &ASEPA_Character::OnlyWalkPress);		//X
	PlayerInputComponent->BindAction("OnlyCrouch",	IE_Pressed, this, &ASEPA_Character::OnlyCrouchPress);	//C


	PlayerInputComponent->BindAction("Jump",	IE_Pressed,	this, &ASEPA_Character::Jump);
	PlayerInputComponent->BindAction("Jump",	IE_Released,this, &ASEPA_Character::StopJumping);
	PlayerInputComponent->BindAction("Sprint",	IE_Pressed,	this, &ASEPA_Character::SprintPress);			//SHIFT
	PlayerInputComponent->BindAction("Sprint",	IE_Released,this, &ASEPA_Character::SprintRelease);			//SHIFT
	PlayerInputComponent->BindAction("Crouch", IE_Pressed,	this, &ASEPA_Character::CrouchPress);			//CTRL
	PlayerInputComponent->BindAction("Crouch", IE_Released,	this, &ASEPA_Character::CrouchRelease);			//CTRL
	//PlayerInputComponent->BindAction("Roll", IE_Pressed,	this, &ASEPA_Character::RollPress);			//CTRL after SHIFT
	//PlayerInputComponent->BindAction("Roll", IE_Released,	this, &ASEPA_Character::RollRelease);		//CTRL
	//PlayerInputComponent->BindAction("HoldBreath", IE_Pressed,	this, &ASEPA_Character::HoldBreathPress);	//SHIFT when we: stand still, crouch and prone
	//PlayerInputComponent->BindAction("HoldBreath", IE_Released,	this, &ASEPA_Character::HoldBreathRelease);	//SHIFT

}

void ASEPA_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//if (HasAuthority())
	//{
	//	StaminaOnSprintTick(DeltaTime);
	//	StaminaOnHoldBreathTick(DeltaTime);
	//	PickUpDropTick(DeltaTime);
	//	SwitchWeaponTick(DeltaTime);
	//}

	if (StaminaComponent)
	{
		float curStamina = StaminaComponent->GetStaminaPercent();
		if (curStamina <= 0.f)
		{
			if (CurrentMovementState == EMovementState::Run_State || CurrentMovementState == EMovementState::Sprint_State)
			{
				//beforRun = true;
				OnlyWalk = true;
			}
		}
		//else if (curStamina == 100.f && beforRun == true)
		//{
		//	beforRun = false;
		//	OnlyWalk = false;
		//}
		//else
		//{
			if (CharacterInMovement())
			{
				if (CurrentMovementState == EMovementState::Run_State || CurrentMovementState == EMovementState::Sprint_State)
				{

					StaminaComponent->SetCharacterMakeMoreEffort(true);
					StaminaComponent->SetEnableStaminaRecovery(false);
					StaminaComponent->SetCoefficientOfChangeStamina(CurrentMovementState, CurrentPositionState, false);

				}
				else
				{

					StaminaComponent->SetCharacterMakeMoreEffort(false);
					//StaminaComponent->SetEnableStaminaRecovery(false);
					StaminaComponent->SetCoefficientOfChangeStamina(CurrentMovementState, CurrentPositionState, true);

				}
			}
			else
			{

				StaminaComponent->SetCharacterMakeMoreEffort(false);
				StaminaComponent->SetCoefficientOfChangeStamina(CurrentMovementState, CurrentPositionState, true);

			}
		//}
	}
}

//void ASEPA_Character::InitWeapon(EWeaponType WeaponType)
void ASEPA_Character::InitWeapon()
{
	////temp: we have to add weapons to slot #2
	//FWeaponSlot WeaponSlot;
	//WeaponSlot.WeaponType = EWeaponType::Rifle_Type;
	//WeaponSlot.NumberOfUnits = 30;
	//InventoryComponent->PickUpWeapon(SlotNumber_CurrentWeapon, WeaponSlot);
	CurrentWeaponState = EWeaponState::ReadyToUse_State;

	USEPA_GameInstance* myGI = Cast<USEPA_GameInstance>(GetGameInstance());
	FWeaponInfo FullWeaponInfo;
	if (myGI)
	{
		//EWeaponType New_WeaponType = InventoryComponent->GetWeaponType_BySlotNumber(SlotNumber_CurrentWeapon);
		//if (myGI->GetWeaponBase_ByWeaponType(New_WeaponType, FullWeaponInfo))
		if (myGI->GetWeaponBase_ByWeaponType(EWeaponType::Rifle_Type, FullWeaponInfo))
		{
			if (FullWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0.f, 90.f, 0.f);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				//ASEPA_WeaponBase* curWeapon = Cast<ASEPA_WeaponBase>(GetWorld()->SpawnActor(FullWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				CurrentWeapon = Cast<ASEPA_WeaponBase>(GetWorld()->SpawnActor(FullWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (CurrentWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					CurrentWeapon->AttachToComponent(GetMesh(), Rule, FName("hand_r_weapon"));
					//CurrentWeapon->SetActorRelativeLocation(FVector(-18.f, 2.f, -7.f));		// Position the weapon
					//CurrentWeapon->SetActorRelativeRotation(FRotator(-10.f, 180.f, 4.f));	// Rotation the weapon
					CurrentWeapon->SetActorRelativeLocation(FVector(0.f, 0.f, 0.f));		// Position the weapon
					CurrentWeapon->SetActorRelativeRotation(FRotator(0.f, 180.f, 0.f));	// Rotation the weapon

					CurrentWeapon->SetWeaponInfo(FullWeaponInfo);
				}
				
			}
			//SwitchWeaponTimer = temp_GetTimer_SwitchWeapon(FullWeaponInfo);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

//EWeaponState ASEPA_Character::GetCurrentWeaponState()
//{
//	EWeaponState CVWS;
//	if (CurrentWeapon)
//	{
//		CVWS = CurrentWeapon->CurrentWeaponState;
//	}
//	else
//	{
//		CVWS = EWeaponState::WithoutWeapon_State;
//	}
//	
//	return CVWS;
//}

//void ASEPA_Character::SetCurrentWeaponState(EWeaponState NewWeaponState)
//{
//	if (CurrentWeapon)
//	{
//		CurrentWeapon->CurrentWeaponState = NewWeaponState;
//	}
//}

bool ASEPA_Character::CharacterInMovement()
{
	bool result = false;

	if (VerticalMovement != FVector::ZeroVector || HorizontalMovement != FVector::ZeroVector)
	{
		result = true;
	}

	return result;
}

bool ASEPA_Character::MovementOnlyForward()
{
	bool resul = false;

	if (VerticalMovement == FVector::ForwardVector && HorizontalMovement == FVector::ZeroVector)
	{
		resul = true;
	}
	else
	{
		MinTime_SprintToRoll = 0.f;
	}

	return resul;
}

//int8 ASEPA_Character::GetCurrentHealthLevel()
//{
//	int8 HealthLevel = 0;
//	if (HealthComponent)
//	{
//		HealthLevel = HealthComponent->HealthLevel;
//	}
//
//	return HealthLevel;
//}

//float ASEPA_Character::temp_GetTimer_SwitchWeapon(FWeaponInfo FullWeaponInfo)
//{
//	float result = 0.f;
//	
//	result = FullWeaponInfo.SwitchWeapon_TimePutInInventory + FullWeaponInfo.SwitchWeapon_TimeTakeFromInventory;
//	
//	return result;
//}

//void ASEPA_Character::ChangePercentMaxSpeed_ByHealthLevel()
//{
//	int8 HealthLevel = GetCurrentHealthLevel();
//
//	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
//	if (myGameInst)
//	{
//		PercentMaxSpeed = myGameInst->GetMovementSpeed_ByHealthLevel(HealthLevel);
//	}
//}

void ASEPA_Character::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		VerticalMovement = GetActorForwardVector();
		//// add movement in that direction
		//AddMovementInput(GetActorForwardVector(), Value);
		AddMovementInput(VerticalMovement, Value);

		ChangeMovementState();
	}
	else
	{
		VerticalMovement = FVector::ZeroVector;
		ChangeMovementState();
	}
}

void ASEPA_Character::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		HorizontalMovement = GetActorRightVector();
		//// add movement in that direction
		//AddMovementInput(GetActorRightVector(), Value);
		AddMovementInput(HorizontalMovement, Value);

		ChangeMovementState();
	}
	else
	{
		HorizontalMovement = FVector::ZeroVector;
		ChangeMovementState();
	}
}

//void ASEPA_Character::TurnAtRate(float Rate)
//{
//	// calculate delta for this frame from the rate information
//	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
//}
//
//void ASEPA_Character::LookUpAtRate(float Rate)
//{
//	// calculate delta for this frame from the rate information
//	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
//}

void ASEPA_Character::FirePress()
{
	if (HealthComponent->GetHealthPercent() > 0.f)
	{
		if (CurrentWeaponState == EWeaponState::ReadyToUse_State && !CheckConditionsByMovement())
		{
			CurrentWeaponState = EWeaponState::Fire_State;
			if (CurrentWeapon)
			{
				CurrentWeapon->Fire_Start();
			}
		}
	}
}

void ASEPA_Character::FireRelease()
{
	CurrentWeaponState = EWeaponState::ReadyToUse_State;
	if (CurrentWeapon)
	{
		CurrentWeapon->Fire_Finish();
	}
}

void ASEPA_Character::AimPress()
{
	//MyComment: we leave priority for aiming, except for two states
	if (CurrentMovementState != EMovementState::Roll_State && CurrentMovementState != EMovementState::ThrowToSide_State)
	{
		bIsTemp_Aim = true;

		if (CharacterInMovement())
		{
			if (CurrentPositionState == EPositionState::Stand_State)
			{
				if (CurrentMovementState == EMovementState::Run_State || CurrentMovementState == EMovementState::Sprint_State)
				{
					CurrentMovementState = EMovementState::Walk_State;
				}
				//else if (CurrentPositionState == EPositionState::Crouch_State || CurrentPositionState == EPositionState::LyingDown_State)
				//{
				//	CurrentMovementState = EMovementState::StandStill_State;
				//}
			}
		}
	}
}

void ASEPA_Character::AimRelease()
{
	if (bIsTemp_Aim)
	{
		bIsTemp_Aim = false;
		bIsTemp_HoldBreath = false;
	}
}

void ASEPA_Character::ReloadPress()
{
	if (CurrentWeaponState == EWeaponState::ReadyToUse_State && CheckConditionsByMovement())
	{
		Reload_Start();
	}
}

void ASEPA_Character::SwitchNextWeaponPress()
{
	SwitchWeaponStart(true);
}

void ASEPA_Character::SwitchPreviosWeaponPress()
{
	SwitchWeaponStart(false);
}

void ASEPA_Character::SwitchWeaponKit1()
{
	SlotNumber_CurrentWeapon = 1;
	SwitchWeapon_BySlotNumber();
}

void ASEPA_Character::SwitchWeaponKit2()
{
	SlotNumber_CurrentWeapon = 2;
	SwitchWeapon_BySlotNumber();
}

void ASEPA_Character::SwitchWeaponKit3()
{
	SlotNumber_CurrentWeapon = 3;
	SwitchWeapon_BySlotNumber();
}

void ASEPA_Character::SwitchWeaponKit4()
{
	SlotNumber_CurrentWeapon = 4;
	SwitchWeapon_BySlotNumber();
}

void ASEPA_Character::SwitchWeaponKit5()
{
	SlotNumber_CurrentWeapon = 5;
	SwitchWeapon_BySlotNumber();
}

void ASEPA_Character::SwitchWeaponKit6()
{
	UseFAItem(ESizeBox::Big_Box);
}

void ASEPA_Character::SwitchWeaponKit7()
{
	UseFAItem(ESizeBox::Medium_Box);
}

void ASEPA_Character::SwitchWeaponKit8()
{
	UseFAItem(ESizeBox::Small_Box);
}

void ASEPA_Character::SwitchWeaponKit9()
{
}

void ASEPA_Character::UseFAItem(ESizeBox SizeBox)
{
	int32 NumberSlotFA = InventoryComponent->GetNumberSlotFA(SizeBox);

	int32 countFA = InventoryComponent->GetRemainderFirstAid_BySlotNumber(NumberSlotFA);
	//bool haveFA = InventoryComponent->CheckBeforeReduce(countFA, 0);
	if (countFA > 0)
	{
		int32 NumberOfUnits = 0;
		USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
		if (myGameInst)
		{
			NumberOfUnits = myGameInst->GetNumberOfUnits(EItemType::FirstAid_Type, EWeaponType::None_Type, SizeBox);
		}

		HealthComponent->RestoreWithFirstAid(NumberOfUnits);
		InventoryComponent->ReduceByItemBox(EItemType::FirstAid_Type, EWeaponType::None_Type, SizeBox);
	}
}

void ASEPA_Character::SwitchWeapon_BySlotNumber()
{
	if (CurrentWeaponState == EWeaponState::ReadyToUse_State && CheckConditionsByMovement())
		//if (GetCurrentWeaponState() == EWeaponState::ReadyToUse_State && CheckConditionsByMovement())
	{
		CurrentWeaponState = EWeaponState::Switch_State;
		////SetCurrentWeaponState(EWeaponState::Switch_State);
		//int32 Old_SlotNumber = SlotNumber_CurrentWeapon;

		//SwitchWeapon(NextWeapon);

		//SwitchWeaponTimer = CurrentWeapon->GetTimePut(Old_SlotNumber) + CurrentWeapon->GetTimeTake(SlotNumber_CurrentWeapon);

		//if (CurrentWeapon)
		//{
		//	FWeaponSlot WeaponSlot = InventoryComponent->SlotsWeapon[SlotNumber_CurrentWeapon];
		//	if (WeaponSlot.WeaponType != EWeaponType::None_Type)
		//	{
		//		//CurrentWeapon->SwitchWeapon_Start(NextWeapon);
		//		CurrentWeapon->SwitchWeapon_Start(SlotNumber_CurrentWeapon);
		//	}
		//}
	}
}

void ASEPA_Character::PickUpWeaponPress()
{
	bIsPickUpDrop_CurrentWeapon = true;
}

void ASEPA_Character::PickUpWeaponRelease()
{
	PickUpDrop_Reset();
}

void ASEPA_Character::SprintPress()
{
	//if ((MovementOnlyForward() && GetCurrentWeaponState() == EWeaponState::ReadyToUse_State) 
	//if ((CharacterInMovement() && GetCurrentWeaponState() == EWeaponState::ReadyToUse_State)
	//	&& !bIsTemp_Aim
	//	&& (CurrentMovementState == EMovementState::Walk_State || CurrentMovementState == EMovementState::Run_State)
	//	&& CanMakeSpecialMovement_ByHealthLevel())
	//{
		CurrentMovementState = EMovementState::Sprint_State;
		bIsTemp_Sprint = true;

		////TODO:	we must control the time in sprint mode
		//CurrentTime_Sprint = 0.f;
	//}
}

void ASEPA_Character::SprintRelease()
{
	bIsTemp_Sprint = false;
}

void ASEPA_Character::OnlyWalkPress()
{
	//beforRun = false;
	CurrentPositionState = EPositionState::Stand_State;
	if (!OnlyWalk)
	{
		OnlyWalk = true;
		OnlyCrouch = false;
		OnlyProne = false;
	}
	else
	{
		OnlyWalk = false;
	}
}

void ASEPA_Character::OnlyCrouchPress()
{
	//beforRun = false;
	if (!OnlyCrouch)
	{
		OnlyWalk = false;
		OnlyCrouch = true;
		OnlyProne = false;
		CurrentPositionState = EPositionState::Crouch_State;
	}
	else
	{
		OnlyCrouch = false;
		CurrentPositionState = EPositionState::Stand_State;
	}
}

void ASEPA_Character::OnlyPronePress()
{
	//beforRun = false;
	if (!OnlyProne)
	{
		OnlyWalk = false;
		OnlyCrouch = false;
		OnlyProne = true;
		CurrentPositionState = EPositionState::LyingDown_State;
	}
	else
	{
		OnlyProne = false;
		CurrentPositionState = EPositionState::Stand_State;
	}
}

void ASEPA_Character::CrouchPress()
{
	if (!OnlyCrouch || !OnlyProne)
	{
		bIsTemp_Crouch = true;
		CurrentPositionState = EPositionState::Crouch_State;
	}
}

void ASEPA_Character::CrouchRelease()
{
	bIsTemp_Crouch = false;
	CurrentPositionState = EPositionState::Stand_State;
}

void ASEPA_Character::RollPress()
{
	////TODO:	we must additional condition for control the achievement of the minimum time in sprint mode to complete the roll
	//if (CurrentMovementState == EMovementState::Sprint_State && CurrentTime_Sprint >= MinTime_SprintToRoll)
	//{
	//	CurrentMovementState = EMovementState::Roll_State;

	//	CurrentTime_Roll = 0.f;
	//}
}

void ASEPA_Character::RollRelease()
{
	////MyComment: at end of animation, our character stand still in crouch
	//if (CurrentMovementState == EMovementState::Roll_State)
	//{
	//	OnlyCrouch = true;
	//	CurrentMovementState = EMovementState::Crouch_State;
	//}
}

void ASEPA_Character::HoldBreathPress()
{
	if (bIsTemp_Aim && !CharacterInMovement())
	{
		//TODO:	we must automatically end this mode when the maximum time is reached
		bIsTemp_HoldBreath = true;
	}
}

void ASEPA_Character::HoldBreathRelease()
{
	bIsTemp_HoldBreath = false;
}

bool ASEPA_Character::CheckConditionsByMovement()
{
	//TODO: add fly (jump or fall)
	bool result = false;
	if (CurrentMovementState == EMovementState::Roll_State
		|| CurrentMovementState == EMovementState::ThrowToSide_State
		|| CurrentMovementState == EMovementState::Sprint_State)
		result = true;

	return result;
}

bool ASEPA_Character::CanMakeSpecialMovement_ByHealthLevel()
{
	//MyComment: we can't make special movement(Sprint, ThrowToSide and Roll), if our HealthLevel < 4
	bool result = false;

	int32 HealthLevel = HealthComponent->GetHealthLevel();
	//int8 HealthLevel = GetCurrentHealthLevel();
	result = (HealthLevel >= 4);

	return result;
}

void ASEPA_Character::ChangeMovementState()
{
	switch (CurrentPositionState)
	{
	case EPositionState::Stand_State:
		if (CharacterInMovement())
		{
			if ((OnlyWalk && !bIsTemp_Sprint) || bIsTemp_Aim)
			{
				CurrentMovementState = EMovementState::Walk_State;
			}
			else if (bIsTemp_Sprint && CheckMaxWeight_RunSprint())
			{
				CurrentMovementState = EMovementState::Sprint_State;
			}
			else
			{
				CurrentMovementState = EMovementState::Run_State;
			}
		}
		else
		{
			CurrentMovementState = EMovementState::StandStill_State;
		}

		break;
	case EPositionState::Crouch_State:
		if (CharacterInMovement())
		{
			if ((OnlyCrouch || bIsTemp_Crouch) && !bIsTemp_Aim)
			{
				CurrentMovementState = EMovementState::Crouch_State;
			}
			else
			{
				//TODO: squat in place
				CurrentMovementState = EMovementState::StandStill_State;
			}
		}
		else
		{
			//TODO: squat in place
			CurrentMovementState = EMovementState::StandStill_State;
		}

		break;
	case EPositionState::LyingDown_State:
		if (CharacterInMovement())
		{
			if (OnlyProne && !bIsTemp_Aim)
			{
				CurrentMovementState = EMovementState::Prone_State;
			}
			else
			{
				//TODO: squat in place
				CurrentMovementState = EMovementState::StandStill_State;
			}
		}
		else
		{
			//TODO: lie still
			CurrentMovementState = EMovementState::StandStill_State;
		}

		break;
	default:
		break;
	}

	ChangeSpeedMovement();
}

float ASEPA_Character::GetMovementSpeed()
{
	float MaxSpeed = 600.0f;

	switch (CurrentMovementState)
	{
	case EMovementState::StandStill_State:
		MaxSpeed = MovementSpeedInfo.StandStill_Speed;
		break;
	case EMovementState::Prone_State:
		MaxSpeed = MovementSpeedInfo.Prone_Speed;
		break;
	case EMovementState::Crouch_State:
		MaxSpeed = MovementSpeedInfo.Crouch_Speed;
		break;

	case EMovementState::Walk_State:
		MaxSpeed = MovementSpeedInfo.Walk_Speed;
		break;
	case EMovementState::Run_State:
		MaxSpeed = MovementSpeedInfo.Run_Speed;
		break;
	case EMovementState::Sprint_State:
		MaxSpeed = MovementSpeedInfo.Sprint_Speed;
		break;

	default:
		break;
	}

	return MaxSpeed;
}

//void ASEPA_Character::ChangeCurrentValues_ByHealthLevel(int32 HealthLevel)
void ASEPA_Character::ChangeCurrentValues_ByHealthLevel()
{
	int32 HealthLevel = HealthComponent->GetHealthLevel();
	FHealthLevelSettings ReturnStructureSettings;

	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		ReturnStructureSettings = myGameInst->GetCaracterAbilities(HealthLevel);
	}

	//PercentMaxAccuracy				= ReturnStructureSettings.PercentageChange_Accuracy;
	PercentMaxSpeed						= ReturnStructureSettings.PercentageChange_MovementSpeed;
	CurrentMaxTime_HoldBreath			= ReturnStructureSettings.MaxTime_HoldBreath;
	//CurrentMaxTime_CooldownHoldBreath	= ReturnStructureSettings.MaxTime_CooldownHoldBreath;
	//CurrentMaxTime_Sprint				= ReturnStructureSettings.MaxTime_Sprint;
	//CurrentMaxTime_CooldownSprint		= ReturnStructureSettings.MaxTime_CooldownSprint;

	CurrentNormalWeight_ForRunSprint	= ReturnStructureSettings.NormalWeight_ForRunSprint;
	CurrentMaxWeight_ForRunSprint		= ReturnStructureSettings.MaxWeight_ForRunSprint;

	ChangeSpeedMovement();
}

//void ASEPA_Character::ChangeCurrentValues_ByHealth(float CurrentHealth)
//{
//	HealthComponent->GetHealthLevel();
//}

float ASEPA_Character::CheckOverweight()
{
	float result = 0.f;
	float TotalWeightWeapon = InventoryComponent->GetTotalWeightWeapon();

	result = TotalWeightWeapon - CurrentNormalWeight_ForRunSprint;

	return result;
}

bool ASEPA_Character::CheckMaxWeight_RunSprint()
{
	bool result = false;
	float TotalWeightWeapon = InventoryComponent->GetTotalWeightWeapon();

	if (TotalWeightWeapon < CurrentMaxWeight_ForRunSprint)
	{
		result = true;
	}

	return result;
}

void ASEPA_Character::ChangeSpeedMovement()
{
	float MaxSpeed = GetMovementSpeed();

	float Overweight = CheckOverweight();
	float ReducingSpeed = 0.f;

	if (Overweight > 0)
	{
		//MyComment: for every 4 kg. overweight, reduce the speed by 2 units
		//ReducingSpeed = (Overweight / 4) * 4;
		ReducingSpeed = Overweight;
	}

	GetCharacterMovement()->MaxWalkSpeed = (MaxSpeed * (PercentMaxSpeed / 100.f)) - ReducingSpeed;

	if (HealthComponent)
	{
		float Coeff = 0.f;
		Coeff = HealthComponent->Get_CoefficientOfRegeneration(CurrentMovementState, CurrentPositionState);
		HealthComponent->Set_CoefficientOfRegeneration(Coeff);
	}

}

void ASEPA_Character::ChangePositionState()
{
	CurrentPositionState = EPositionState::Stand_State;
}


//void ASEPA_Character::StaminaOnSprintTick(float DeltaTime)
//{
//	if (CurrentMovementState == EMovementState::Sprint_State || bIsTemp_Sprint)
//	{
//		if (CurrentTime_Sprint <= CurrentMaxTime_Sprint)
//		{
//			CurrentTime_Sprint += DeltaTime;
//			CurrentTime_CooldownSprint -= DeltaTime;
//		}
//	}
//	else
//	{
//		if (CurrentTime_CooldownSprint <= CurrentMaxTime_CooldownSprint)
//		{
//			bIsTemp_Sprint = false;
//			CurrentTime_Sprint = 0.f;
//			CurrentTime_CooldownSprint += DeltaTime;
//		}
//	}
//}

//void ASEPA_Character::StaminaOnHoldBreathTick(float DeltaTime)
//{
//	if (bIsTemp_HoldBreath)
//	{
//		if (CurrentTime_HoldBreath <= CurrentMaxTime_HoldBreath)
//		{
//			CurrentTime_HoldBreath += DeltaTime;
//			CurrentTime_CooldownHoldBreath -= DeltaTime;
//		}
//	}
//	else
//	{
//		if (CurrentTime_CooldownHoldBreath <= CurrentMaxTime_CooldownHoldBreath)
//		{
//			bIsTemp_HoldBreath = false;
//			CurrentTime_HoldBreath -= DeltaTime;
//			CurrentTime_CooldownHoldBreath += DeltaTime;
//		}
//	}
//}

//void ASEPA_Character::Reload_Start(UAnimMontage* Anim)
void ASEPA_Character::Reload_Start()
{
	// 1. �������� ������� � ��������,
	// 2. ������� ������������ ���������� �������� ��� �������� ������,
	// 3. ��������� ����� �� ��������� ����������� (�������� �����, �������� ���������),
	// 4. �������� ������ ��� �����������,
	// 5. �������� �������� �����������.
	// 6. ��������, ��� �� ��� ��������, ����� �������� ����������.
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	FWeaponInfo cWeaponInfo = CurrentWeapon->WeaponInfo;

	//1.
	int RemainderAmmo_InInventory = 0;
	USEPA_InventoryComponent* MyInventory = Cast<USEPA_InventoryComponent>(GetOwner()->GetComponentByClass(USEPA_InventoryComponent::StaticClass()));
	if (MyInventory)
	{
		if (CurrentWeapon)
		{
			RemainderAmmo_InInventory = MyInventory->GetRemainderAmmo_ByWeaponType(CurrentWeapon->WeaponType);
		}
	}
	if (RemainderAmmo_InInventory > 0)
	{
		CurrentWeaponState = EWeaponState::Reload_State;
		//2.
		int32 RemainderAmmo_InWeapon = 0;
		int32 MaxAmmo = 0;
		int32 RemainderAmmo_toReload = 0;
		if (CurrentWeapon)
		{
			RemainderAmmo_InWeapon = CurrentWeapon->NumberOfBullets;
		}
		//MaxAmmo = cWeaponInfo.MaxNumberOfBullets;
		//int32 cNumberOfBullets = MinElement(RemainderAmmo_InInventory, MaxAmmo);
		//RemainderAmmo_toReload = cNumberOfBullets - RemainderAmmo_InWeapon;

		//3.
		float TotalTimeToReload = 0.f;
		TotalTimeToReload = cWeaponInfo.ReloadTimer;

		float TimeToReload = 2.f;
		//float TimeToReload = (RemainderAmmo_toReload * 100) / TotalTimeToReload;
		TimeToReload = TotalTimeToReload / 2;
	
		//4.
		//p1. no clip in the weapon
		GetWorld()->GetTimerManager().SetTimer(Timer_Reload, this, &ASEPA_Character::Reload_Finish, TimeToReload, false);
		//p2. there are clips in the weapon
		GetWorld()->GetTimerManager().SetTimer(Timer_Reload, this, &ASEPA_Character::Reload_Finish, TimeToReload, false);

		//WeaponReloadStart_BP(Anim);
	}
}

//void ASEPA_Character::Reload_Finish(int32 RemainderAmmo_toReload)
void ASEPA_Character::Reload_Finish()
{
	CurrentWeaponState = EWeaponState::ReadyToUse_State;

	//������� ������������ ���������� �������� �� ���������
	int32 MaxNumberOfBullets = CurrentWeapon->WeaponInfo.MaxNumberOfBullets;
	//InventoryComponent->ReduceAfterReload_Ammo(CurrentWeapon->WeaponType, RemainderAmmo_toReload);
	GetWorld()->GetTimerManager().ClearTimer(Timer_Reload);
}

//void ASEPA_Character::WeaponReloadFinish(bool bIsSuccess, int32 AmmoSafe)
//{
//	SetCurrentWeaponState(EWeaponState::ReadyToUse_State);
//
//	//WeaponReloadFinish_BP(bIsSuccess);
//}

//void ASEPA_Character::SwitchWeaponTick(float DeltaTime)
//{
//	if (GetCurrentWeaponState() == EWeaponState::Switch_State)
//	{
//		if (SwitchWeaponTimer < 0.0f)
//		{
//			SwitchWeaponFinish();
//		}
//		else
//		{
//			SwitchWeaponTimer -= DeltaTime;
//		}
//	}
//}

void ASEPA_Character::SwitchWeaponStart(bool NextWeapon)
{
	if (NextWeapon)
	{
		if (SlotNumber_CurrentWeapon == 5)
		{
			SlotNumber_CurrentWeapon = 1;
		}
		else
		{
			SlotNumber_CurrentWeapon++;
		}
	}
	else
	{
		if (SlotNumber_CurrentWeapon == 1)
		{
			SlotNumber_CurrentWeapon = 5;
		}
		else
		{
			SlotNumber_CurrentWeapon--;
		}
	}

	SwitchWeapon_BySlotNumber();
}

//void ASEPA_Character::SwitchWeaponFinish(bool bIsSuccess)
void ASEPA_Character::SwitchWeaponFinish()
{
	//SetCurrentWeaponState(EWeaponState::ReadyToUse_State);
	//if (CurrentWeapon)
	//{
	//	//InitWeapon(CurrentWeapon->WeaponInfo->WeaponType);
		InitWeapon();
	//}
}

//void ASEPA_Character::PickUpDropTick(float DeltaTime)
//{
//	if (bIsPickUpDrop_CurrentWeapon)
//	{
//		if (Time_PickUpDrop_CurrentWeapon < 0.0f)
//		{
//			PickUpDrop_TimeExecute();
//			PickUpDrop_Reset();
//		}
//		else
//		{
//			Time_PickUpDrop_CurrentWeapon -= DeltaTime;
//		}
//	}
//}

void ASEPA_Character::PickUpDrop_TimeExecute()
{
	//TODO: for pick up we have to make pause 1 sec.
	//TODO: Animation play for pick up and drop
	//InventoryComponent->PickUpDrop_CurrentWeapon(SlotNumber_CurrentWeapon);
}

void ASEPA_Character::PickUpDrop_Reset()
{
	bIsPickUpDrop_CurrentWeapon = false;
	Time_PickUpDrop_CurrentWeapon = 2.f;
}


void ASEPA_Character::CharacterDead()
{
	//switch (GetCurrentWeaponState())
	//{
	//case EWeaponState::Reload_State:
	//	
	//	//CurrentWeapon->ExecutionAborted_Reload();
	//	break;
	//case EWeaponState::Switch_State:

	//	//CurrentWeapon->ExecutionAborted_SwitchWeapon();
	//	break;

	//default:
	//	break;
	//}

	if (HasAuthority())
	{
		CurrentWeaponState = EWeaponState::ReadyToUse_State;
		CurrentWeapon->Fire_Finish();
		float TimeDeadAnim = 0.0f;
	//	int32 rnd = FMath::RandHelper(DeadAnim.Num());

	//	if (DeadAnim.IsValidIndex(rnd) && DeadAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	//	{
	//		//GetMesh()->GetAnimInstance()->Montage_Play(DeadAnim[rnd]);
	//		PlayAnim_Multicast(DeadAnim[rnd]);
	//	}

	//	TimeDeadAnim = DeadAnim[rnd]->GetPlayLength();
	//	//bIsAlive = false;

		if (GetController())
		{
			GetController()->UnPossess();
		}
	
	//	//Timer ragdoll
	//	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATPSCharacter::EnableRagDoll_Multicast, TimeDeadAnim, false);

	}
	//else
	//{
	//	AttackCharEvent(false);
	//}

	//CharDead_BP();
}

void ASEPA_Character::EnableRagDoll()
{

	//UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::EnableRagDoll"));

	if (GetMesh())
	{
		if (GetCapsuleComponent())
		{
			GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
		}
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ASEPA_Character::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (ProtectionComponent && ProtectionComponent->GetProtectionPercent() > 0.f)
	{
		ProtectionComponent->ChangeProtection_OnDamage(DamageAmount);
		float CoefficientOfDamage = ProtectionComponent->GetCoefficientOfDamage();
		if (CoefficientOfDamage > 0.f)
		{
			float ValueDamage = DamageAmount * CoefficientOfDamage;
			if (HealthComponent && HealthComponent->GetIsAlive())
			{
				HealthComponent->ChangeHealth_OnDamage(ValueDamage);
			}
		}
	}
	else
	{
		if (HealthComponent && HealthComponent->GetIsAlive())
		{
			HealthComponent->ChangeHealth_OnDamage(DamageAmount);
		}
	}

	//if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	//{
	//	ASEPA_Projectile* myProjectile = Cast<ASEPA_Projectile>(DamageCauser);
	//	if (myProjectile)
	//	{
	//		//UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfaceType());	//to do Name_None - bone for radial damage
	//	}
	//}

	return ActualDamage;
}

//void ASEPA_Character::RunGC()
//{
//	GEngine->ForceGarbageCollection(true);
//
//}






void ASEPA_Character::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//	DOREPLIFETIME(AWeaponDefault, WeaponInfo);
	//	DOREPLIFETIME(AWeaponDefault, WeaponReloading);
	DOREPLIFETIME(ASEPA_Character, CurrentWeapon);

}