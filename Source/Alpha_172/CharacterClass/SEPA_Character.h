// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Alpha_172/GeneralClass/SEPA_Types.h"
#include "Alpha_172/ComponentsActor/SEPA_HealthComponent.h"
#include "Alpha_172/ComponentsActor/SEPA_InventoryComponent.h"
#include "Alpha_172/ComponentsActor/SEPA_StaminaComponent.h"
#include "Alpha_172/ComponentsActor/SEPA_ProtectionComponent.h"
#include "Alpha_172/ComponentsActor/SEPA_WeaponComponent.h"
#include "SEPA_Character.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
//class UMotionControllerComponent;
//class UAnimMontage;
//class USoundBase;

UCLASS(config=Game)
class ASEPA_Character : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	//UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	//USkeletalMeshComponent* Mesh1P;

	///** Gun mesh: 1st person view (seen only by self) */
	//UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	//USkeletalMeshComponent* FP_Gun;

	///** Location on gun mesh where projectiles should spawn. */
	//UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	//USceneComponent* FP_MuzzleLocation;

	///** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	//UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	//USkeletalMeshComponent* VR_Gun;

	///** Location on VR gun mesh where projectiles should spawn. */
	//UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	//USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	///** Motion controller (right hand) */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	//UMotionControllerComponent* R_MotionController;

	///** Motion controller (left hand) */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	//UMotionControllerComponent* L_MotionController;

protected:
	virtual void BeginPlay();

private:
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	//	class USEPA_InventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USEPA_HealthComponent* HealthComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USEPA_InventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USEPA_StaminaComponent* StaminaComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USEPA_ProtectionComponent* ProtectionComponent;
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Actor)
	//	class ASEPA_WeaponBase* WeaponBase;

public:

	ASEPA_Character();

	///** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	//float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	///** Gun muzzle's offset from the characters location */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	//FVector GunOffset;

	///** Projectile class to spawn */
	//UPROPERTY(EditDefaultsOnly, Category=Projectile)
	//TSubclassOf<class ASEPA_Projectile> ProjectileClass;

	///** Sound to play each time we fire */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	//USoundBase* FireSound;

	///** AnimMontage to play each time we fire */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	//UAnimMontage* FireAnimation;

	///** Whether to use motion controller location for aiming. */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	//uint8 bUsingMotionControllers : 1;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	///** Returns Mesh1P subobject **/
	//USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(Replicated)
		ASEPA_WeaponBase* CurrentWeapon = nullptr;
	UPROPERTY()
		int32 SlotNumber_CurrentWeapon = 1;

	//MyComment: for weapon in character
	UFUNCTION(BlueprintCallable)
		//void InitWeapon(EWeaponType WeaponType);
		void InitWeapon();

	//MyComment: for movement
	UPROPERTY(BlueprintReadOnly)
	EMovementState CurrentMovementState;			
	EWeaponState CurrentWeaponState;
	//EWeaponState GetCurrentWeaponState();
	//void SetCurrentWeaponState(EWeaponState NewWeaponState);
	//EDirectionMovingState CurrentDirectionMoving;	
	UPROPERTY(BlueprintReadOnly)
		EPositionState CurrentPositionState;

	FVector VerticalMovement;
	FVector HorizontalMovement;


	UFUNCTION()
		bool CharacterInMovement();
	UFUNCTION()
		bool MovementOnlyForward();
	//UFUNCTION()
	//	int8 GetCurrentHealthLevel();
	//UFUNCTION()
	//	void ChangePercentMaxSpeed_ByHealthLevel();

	//bool beforRun = false;

	bool OnlyWalk = false;
	bool OnlyCrouch = false;
	bool OnlyProne = false;

	bool bIsTemp_Aim = false;
	bool bIsTemp_HoldBreath = false;
	bool bIsTemp_Crouch = false;
	bool bIsTemp_Sprint = false;
	//bool bIsRoll = false;
	
	bool bIsPickUpDrop_CurrentWeapon = false;
	float Time_PickUpDrop_CurrentWeapon = 2.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
		float SwitchWeaponTimer = 0.0f;

	//float temp_GetTimer_SwitchWeapon(FWeaponInfo FullWeaponInfo);
	
	//float MaxTime_HoldBreath = 5.f;			//may vary depending on the level of the character or the addition of new abilities
	//float MaxTime_CooldownHoldBreath = 5.f;	//may vary depending on the level of the character or the addition of new abilities
	//float MaxTime_Sprint = 10.f;				//may vary depending on the level of the character or the addition of new abilities
	//float MaxTime_CooldownSprint = 5.f;		//may vary depending on the level of the character or the addition of new abilities

	//float PercentMaxAccuracy = 100.f;
	float PercentMaxSpeed = 100.f;
	//float CurrentTime_Sprint = 0.f;
	//float CurrentTime_CooldownSprint = 0.f;
	//float CurrentTime_HoldBreath = 0.f;
	//float CurrentTime_CooldownHoldBreath = 0.f;

	float MinTime_SprintToRoll = 2.f;	
	float CurrentTime_Roll = 0.f;
	float MaxTime_Roll = 2.f;
	
	float CurrentMaxTime_HoldBreath = 0.f;
	float CurrentMaxTime_CooldownHoldBreath = 0.f;
	//float CurrentMaxTime_Sprint = 0.f;
	//float CurrentMaxTime_CooldownSprint = 0.f;

	float CurrentNormalWeight_ForRunSprint = 0.f;
	float CurrentMaxWeight_ForRunSprint = 0.f;

	void MoveForward(float Val);
	void MoveRight(float Val);
	//void TurnAtRate(float Rate);
	//void LookUpAtRate(float Rate);

	UFUNCTION()
		void FirePress();
	UFUNCTION()
		void FireRelease();

	UFUNCTION()
		void AimPress();
	UFUNCTION()
		void AimRelease();

	UFUNCTION()
		void ReloadPress();
	FTimerHandle Timer_Reload;

	UFUNCTION()
		void SwitchNextWeaponPress();
	UFUNCTION()
		void SwitchPreviosWeaponPress();

	UFUNCTION()
		void SwitchWeaponKit1();
	UFUNCTION()
		void SwitchWeaponKit2();
	UFUNCTION()
		void SwitchWeaponKit3();
	UFUNCTION()
		void SwitchWeaponKit4();
	UFUNCTION()
		void SwitchWeaponKit5();
	UFUNCTION()
		void SwitchWeaponKit6();
	UFUNCTION()
		void SwitchWeaponKit7();
	UFUNCTION()
		void SwitchWeaponKit8();
	UFUNCTION()
		void SwitchWeaponKit9();

	UFUNCTION()
		void UseFAItem(ESizeBox SizeBox);
	UFUNCTION()
		void SwitchWeapon_BySlotNumber();


	UFUNCTION()
		void PickUpWeaponPress();
	UFUNCTION()
		void PickUpWeaponRelease();

	UFUNCTION()
		void OnlyWalkPress();
	UFUNCTION()
		void OnlyCrouchPress();
	UFUNCTION()
		void OnlyPronePress();

	UFUNCTION()
		void CrouchPress();
	UFUNCTION()
		void CrouchRelease();
	UFUNCTION()
		void RollPress();
	UFUNCTION()
		void RollRelease();
	UFUNCTION()
		void SprintPress();
	UFUNCTION()
		void SprintRelease();
	UFUNCTION()
		void HoldBreathPress();
	UFUNCTION()
		void HoldBreathRelease();


	UFUNCTION()
		bool CheckConditionsByMovement();
	UFUNCTION()
		bool CanMakeSpecialMovement_ByHealthLevel();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FMovementSpeed MovementSpeedInfo;

	UFUNCTION()
		void ChangeMovementState();
	UFUNCTION()
		float GetMovementSpeed();
	UFUNCTION()
		void ChangePositionState();

	UFUNCTION()
		//void ChangeCurrentValues_ByHealthLevel(int32 HealthLevel);
		void ChangeCurrentValues_ByHealthLevel();
	//UFUNCTION()
	//	void ChangeCurrentValues_ByHealth(float CurrentHealth);
	UFUNCTION()
		float CheckOverweight();
	UFUNCTION()
		bool CheckMaxWeight_RunSprint();
	UFUNCTION()
		void ChangeSpeedMovement();

	//UFUNCTION()
	//	void StaminaOnSprintTick(float DeltaTime);
	//UFUNCTION()
	//	void StaminaOnHoldBreathTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
		//void Reload_Start(UAnimMontage* Anim);
		void Reload_Start();
	UFUNCTION(BlueprintCallable)
		//void Reload_Finish(int32 RemainderAmmo_toReload);
		void Reload_Finish();

	//UFUNCTION(BlueprintCallable)
	//	void WeaponReloadFinish(bool bIsSuccess, int32 AmmoSafe);
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	//	void WeaponReloadStart_BP(UAnimMontage* Anim);
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	//	void WeaponReloadFinish_BP(bool bIsSuccess);

	//void SwitchWeaponTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
		void SwitchWeaponStart(bool NextWeapon);
	UFUNCTION(BlueprintCallable)
	//	void SwitchWeaponFinish(bool bIsSuccess);
		void SwitchWeaponFinish();
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	//	void SwitchWeaponStart_BP(UAnimMontage* Anim);
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	//	void SwitchWeaponFinish_BP(bool bIsSuccess);

	//UFUNCTION()
	//	void PickUpDropTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
		void PickUpDrop_TimeExecute();
	UFUNCTION()
		void PickUpDrop_Reset();

	UFUNCTION(BlueprintCallable)
		void CharacterDead();
	//UFUNCTION(BlueprintNativeEvent)
	//	void CharacterDead_BP();
	UFUNCTION()
		void EnableRagDoll();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	//UFUNCTION(BlueprintCallable)
	//	void RunGC();

};

