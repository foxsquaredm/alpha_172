// Copyright Epic Games, Inc. All Rights Reserved.

#include "Alpha_172/GameClass/SEPA_GameMode.h"
#include "Alpha_172/GameClass/SEPA_HUD.h"
#include "Alpha_172/CharacterClass/SEPA_Character.h"
#include "UObject/ConstructorHelpers.h"

ASEPA_GameMode::ASEPA_GameMode()
	: Super()
{
	//// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	//DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASEPA_HUD::StaticClass();
}
