// Fill out your copyright notice in the Description page of Project Settings.


#include "SEPA_GameInstance.h"


int32 USEPA_GameInstance::GetNumberOfUnits(EItemType ItemType, EWeaponType WeaponType, ESizeBox SizeBox)
{
	int32 NumberOfUnits = 0;

	int8 i = 0;
	bool bIsFind = false;

	if (TablVolumeInBoxes)
	{
		//FVolumeInBoxes* VolumeInBoxesRow;
		TArray<FVolumeInBoxes*> OutAllRow;
		FString JustString = "NameTable";
		TablVolumeInBoxes->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->ItemType == ItemType 
				&& OutAllRow[i]->WeaponType == WeaponType 
				&& OutAllRow[i]->SizeBox == SizeBox)
						{
							NumberOfUnits = OutAllRow[i]->InBox;
							bIsFind = true;
						}
			i++;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("USEPA_GameInstance::GetNumberOfUnits - TablVolumeInBoxes -NULL"));
	}

	return NumberOfUnits;
}

bool USEPA_GameInstance::GetWeaponBase_ByWeaponType(EWeaponType WeaponType, FWeaponInfo& OutWeaponInfo)
{
	int8 i = 0;
	bool bIsFind = false;

	if (TablWeaponBass)
	{
		TArray<FWeaponInfo*> OutAllRow;
		FString JustString = "NameTable";
		TablWeaponBass->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->WeaponType == WeaponType)
			{
				OutWeaponInfo = *OutAllRow[i];
				bIsFind = true;
			}
			i++;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("USEPA_GameInstance::GetWeaponBase_ByWeaponType - TablWeaponBass -NULL"));
	}

	return bIsFind;
}

float USEPA_GameInstance::GetNumberOfSeats(EWeaponType WeaponType)
{
	float NumberOfSeats = 0.f;

	int8 i = 0;
	bool bIsFind = false;

	if (TablNumberOfSeatsForAmmoSettings)
	{
		//FVolumeInBoxes* VolumeInBoxesRow;
		TArray<FNumberOfSeatsForAmmo*> OutAllRow;
		FString JustString = "NameTable";
		TablNumberOfSeatsForAmmoSettings->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->WeaponType == WeaponType)
			{
				NumberOfSeats = OutAllRow[i]->NumberOfUnits;
				bIsFind = true;
			}
			i++;
		}
	}

	return NumberOfSeats;
}

//TODO: add data for calculation
//FReturnStuct_DispersionSettings USEPA_GameInstance::GetRateOfChange(EMovementState MovementState)
FChangingDispersionSettings USEPA_GameInstance::GetRateOfChange(EMovementState MovementState)
{
	FChangingDispersionSettings StructureDispersionSettings;

	int8 i = 0;
	bool bIsFind = false;

	if (TablChangingDispersionSettings)
	{
		//FVolumeInBoxes* VolumeInBoxesRow;
		TArray<FChangingDispersionSettings*> OutAllRow;
		FString JustString = "NameTable";
		TablChangingDispersionSettings->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->MovementState == MovementState)
			{
				//NumberOfSeats = OutAllRow[i]->NumberOfUnits;
				StructureDispersionSettings.RateOfChange_AimMax = OutAllRow[i]->RateOfChange_AimMax;
				StructureDispersionSettings.RateOfChange_AimMin = OutAllRow[i]->RateOfChange_AimMin;
				StructureDispersionSettings.RateOfChange_AimRecoil = OutAllRow[i]->RateOfChange_AimRecoil;
				StructureDispersionSettings.RateOfChange_Reduction = OutAllRow[i]->RateOfChange_Reduction;
				bIsFind = true;
			}
			i++;
		}
	}

	return StructureDispersionSettings;
}

//float USEPA_GameInstance::GetMovementSpeed_ByHealthLevel(int8 HealthLevel)
//{
//	float MovementSpeed = 0.f;
//
//	int8 i = 0;
//	bool bIsFind = false;
//
//	if (TablHealthLevelSettings)
//	{
//		//FVolumeInBoxes* VolumeInBoxesRow;
//		TArray<FHealthLevelSettings*> OutAllRow;
//		FString JustString = "NameTable";
//		TablHealthLevelSettings->GetAllRows(JustString, OutAllRow);
//
//		while (i < OutAllRow.Num() && !bIsFind)
//		{
//			if (OutAllRow[i]->HealthLevel == HealthLevel)
//			{
//				MovementSpeed = OutAllRow[i]->PercentageChange_MovementSpeed;
//				bIsFind = true;
//			}
//			i++;
//		}
//	}
//
//	return MovementSpeed;
//}

int32 USEPA_GameInstance::GetHealthLevel_fromTabl(float Health)
{
	int32 resultHealthLevel = 0;

	int8 i = 0;
	bool bIsFind = false;

	if (TablHealthLevelSettings)
	{
		//FVolumeInBoxes* VolumeInBoxesRow;
		TArray<FHealthLevelSettings*> OutAllRow;
		FString JustString = "NameTable";
		TablHealthLevelSettings->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->Health_Max >= Health && OutAllRow[i]->Health_Min < Health)
			{
				resultHealthLevel = OutAllRow[i]->HealthLevel;
				bIsFind = true;
			}
			i++;
		}
	}
	
	return resultHealthLevel;
}

FHealthLevelSettings USEPA_GameInstance::GetCaracterAbilities(int32 HealthLevel)
{
	FHealthLevelSettings ReturnStructure_CaracterAbilities;

	int8 i = 0;
	bool bIsFind = false;

	if (TablHealthLevelSettings)
	{
		//FVolumeInBoxes* VolumeInBoxesRow;
		TArray<FHealthLevelSettings*> OutAllRow;
		FString JustString = "NameTable";
		TablHealthLevelSettings->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->HealthLevel == HealthLevel)
			{
				ReturnStructure_CaracterAbilities.PercentageChange_Accuracy			= OutAllRow[i]->PercentageChange_Accuracy;
				ReturnStructure_CaracterAbilities.PercentageChange_MovementSpeed	= OutAllRow[i]->PercentageChange_MovementSpeed;
				ReturnStructure_CaracterAbilities.MaxTime_HoldBreath				= OutAllRow[i]->MaxTime_HoldBreath;
				//ReturnStructure_CaracterAbilities.MaxTime_CooldownHoldBreath		= OutAllRow[i]->MaxTime_CooldownHoldBreath;
				//ReturnStructure_CaracterAbilities.MaxTime_Sprint					= OutAllRow[i]->MaxTime_Sprint;
				//ReturnStructure_CaracterAbilities.MaxTime_CooldownSprint			= OutAllRow[i]->MaxTime_CooldownSprint;

				ReturnStructure_CaracterAbilities.NormalWeight_ForRunSprint			= OutAllRow[i]->NormalWeight_ForRunSprint;
				ReturnStructure_CaracterAbilities.MaxWeight_ForRunSprint			= OutAllRow[i]->MaxWeight_ForRunSprint;

				bIsFind = true;
			}
			i++;
		}
	}

	return ReturnStructure_CaracterAbilities;
}

float USEPA_GameInstance::GetMaxHealth_ByCurrentHealthLevel(int32 HealthLevel)
{
	int32 resultMaxHealth_ByCurrentHealthLevel = 0;

	int8 i = 0;
	bool bIsFind = false;

	if (TablHealthLevelSettings)
	{
		//FVolumeInBoxes* VolumeInBoxesRow;
		TArray<FHealthLevelSettings*> OutAllRow;
		FString JustString = "NameTable";
		TablHealthLevelSettings->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->HealthLevel == HealthLevel)
			{
				resultMaxHealth_ByCurrentHealthLevel = OutAllRow[i]->Health_Max;
				bIsFind = true;
			}
			i++;
		}
	}

	return resultMaxHealth_ByCurrentHealthLevel;
}

float USEPA_GameInstance::GetCoefficientOfRegeneration(EMovementState MovementState, EPositionState PositionState)
{
	float CoefficientOfRegeneration = 0;

	int8 i = 0;
	bool bIsFind = false;

	if (TablHealthRegenerationSettings)
	{
		TArray<FHealthRegenerationSettings*> OutAllRow;
		FString JustString = "NameTable";
		TablHealthRegenerationSettings->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->MovementState == MovementState && OutAllRow[i]->PositionState == PositionState)
			{
				CoefficientOfRegeneration = OutAllRow[i]->CoefficientOfRegeneration;
				bIsFind = true;
			}
			i++;
		}
	}

	return CoefficientOfRegeneration;
}

int32 USEPA_GameInstance::GetProtectionLevel_fromTabl(float Protection)
{
	int32 resultProtectionhLevel = 0;

	int8 i = 0;
	bool bIsFind = false;

	if (TablProtectionLevelSettings)
	{
		//FVolumeInBoxes* VolumeInBoxesRow;
		TArray<FProtectionLevelSettings*> OutAllRow;
		FString JustString = "NameTable";
		TablProtectionLevelSettings->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->Protection_Max >= Protection && OutAllRow[i]->Protection_Min < Protection)
			{
				resultProtectionhLevel = OutAllRow[i]->ProtectionLevel;
				bIsFind = true;
			}
			i++;
		}
	}

	return resultProtectionhLevel;
}

float USEPA_GameInstance::GetCoefficientOfDamage(int32 ProtectionLevel)
{
	float CoefficientOfDamage = 0.f;

	int8 i = 0;
	bool bIsFind = false;

	if (TablProtectionLevelSettings)
	{
		TArray<FProtectionLevelSettings*> OutAllRow;
		FString JustString = "NameTable";
		TablProtectionLevelSettings->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->ProtectionLevel == ProtectionLevel)
			{
				CoefficientOfDamage = OutAllRow[i]->CoefficientOfDamage;
				bIsFind = true;
			}
			i++;
		}
	}

	return CoefficientOfDamage;
}

float USEPA_GameInstance::GetCoefficientOfChangeStamina(EMovementState MovementState, EPositionState PositionState, bool isRecovery)
{
	float CoefficientOfChangeStamina = 0.f;

	int8 i = 0;
	bool bIsFind = false;

	if (TablStaminaSettings)
	{
		TArray<FStaminaSettings*> OutAllRow;
		FString JustString = "NameTable";
		TablStaminaSettings->GetAllRows(JustString, OutAllRow);

		while (i < OutAllRow.Num() && !bIsFind)
		{
			if (OutAllRow[i]->MovementState == MovementState && OutAllRow[i]->PositionState == PositionState && OutAllRow[i]->isRecovery == isRecovery)
			{
				CoefficientOfChangeStamina = OutAllRow[i]->CoefficientOfChange;
				bIsFind = true;
			}
			i++;
		}
	}

	return CoefficientOfChangeStamina;
}
