// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SEPA_GameMode.generated.h"

UCLASS(minimalapi)
class ASEPA_GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASEPA_GameMode();

	UPROPERTY(BlueprintReadOnly)
		bool SEPA_TestingMode = false;

};

