// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Engine/GameInstance.h"
#include "Alpha_172/GeneralClass/SEPA_Types.h"
#include "Alpha_172/ComponentsActor/SEPA_WeaponComponent.h"
#include "SEPA_GameInstance.generated.h"


UCLASS()
class ALPHA_172_API USEPA_GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	//MyComment: data tables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		UDataTable* TablVolumeInBoxes = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		UDataTable* TablWeaponBass = nullptr;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
	//	UDataTable* TablWeaponIcon = nullptr;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
	//	UDataTable* TablHealthLevelSettings = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "InventarSetting")
		UDataTable* TablNumberOfSeatsForAmmoSettings = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DispersionSetting")
		UDataTable* TablChangingDispersionSettings = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterSetting")
		UDataTable* TablHealthLevelSettings = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterSetting")
		UDataTable* TablHealthRegenerationSettings = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterSetting")
		UDataTable* TablProtectionLevelSettings = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterSetting")
		UDataTable* TablStaminaSettings = nullptr;

	//MyComment: queries to data tables
	UFUNCTION(BlueprintCallable)
		int32 GetNumberOfUnits(EItemType ItemType, EWeaponType WeaponType, ESizeBox SizeBox);	//we can use FItemInfo !!!, instead 3 parameters
	//UFUNCTION()
	//	static int32 GetNumberOfUnits(EItemType ItemType, EWeaponType WeaponType, ESizeBox SizeBox);
	UFUNCTION(BlueprintCallable)
		bool GetWeaponBase_ByWeaponType(EWeaponType WeaponType, FWeaponInfo& OutWeaponInfo);

	UFUNCTION(BlueprintCallable)
		float GetNumberOfSeats(EWeaponType WeaponType);	//MyComment: we can use FItemInfo !!!, instead 3 parameters
	UFUNCTION(BlueprintCallable)
		FChangingDispersionSettings GetRateOfChange(EMovementState MovementState);
	////UFUNCTION(BlueprintCallable)
	////	float GetAccuracy_HealthLevelSettings(int32 HealthLevel);
	//UFUNCTION(BlueprintCallable)
	//	float GetMovementSpeed_ByHealthLevel(int8 HealthLevel);
	
	//MyComment: for health
	UFUNCTION(BlueprintCallable)
		int32 GetHealthLevel_fromTabl(float Health);
	UFUNCTION(BlueprintCallable)
		FHealthLevelSettings GetCaracterAbilities(int32 HealthLevel);
	UFUNCTION(BlueprintCallable)
		float GetMaxHealth_ByCurrentHealthLevel(int32 HealthLevel);

	//MyComment: for regeneration
	UFUNCTION(BlueprintCallable)
		float GetCoefficientOfRegeneration(EMovementState MovementState, EPositionState PositionState);
	
	//MyComment: for protection
	UFUNCTION(BlueprintCallable)
		int32 GetProtectionLevel_fromTabl(float Protection);
	UFUNCTION(BlueprintCallable)
		float GetCoefficientOfDamage(int32 ProtectionLevel);

	//MyComment: for stamina
	UFUNCTION(BlueprintCallable)
		float GetCoefficientOfChangeStamina(EMovementState MovementState, EPositionState PositionState, bool isRecovery);

};
