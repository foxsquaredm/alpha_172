// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SEPA_HUD.generated.h"

UCLASS()
class ASEPA_HUD : public AHUD
{
	GENERATED_BODY()

public:
	ASEPA_HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

