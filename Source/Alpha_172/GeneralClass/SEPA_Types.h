// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Engine/Level.h"
#include "SEPA_Types.generated.h"

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	None_Type UMETA(DisplayName				= "NONE"),
	Pistol_Type UMETA(DisplayName			= "Pistol"),
	Rifle_Type UMETA(DisplayName			= "Rifle"),
	//SMG
	ShotGun_Type UMETA(DisplayName			= "ShotGun"),
	//Machine gun
	//Flamethrower
	SniperRifle_Type UMETA(DisplayName		= "SniperRifle"),
	//DMR
	GrenadeLauncher_Type UMETA(DisplayName	= "GrenadeLauncher")
};

UENUM(BlueprintType)
enum class EWeaponModel : uint8
{
	None_Model UMETA(DisplayName = "NONE"),
	AR4_Model UMETA(DisplayName = "AR4"),
	Ka47_Model UMETA(DisplayName = "Ka47"),
	KA74U_Model UMETA(DisplayName = "KA74U"),
	KA_Val_Model UMETA(DisplayName = "KA_Val"),
	SMG11_Model UMETA(DisplayName = "SMG11"),
	M9_Knife_Model UMETA(DisplayName = "M9_Knife"),
	G67_Grenade_Model UMETA(DisplayName = "G67_Grenade")
};


UENUM(BlueprintType)
enum class EMovementState : uint8
{
	StandStill_State UMETA(DisplayName		= "Stand still"),				//without pressing movement
	//StandBehinCover_State UMETA(DisplayName	= "Stand behind cover"),
	//StandStillAim_State UMETA(DisplayName	= "Stand still with aiming"),
	//StandStill_HoldBreath_State	UMETA(DisplayName	= "Stand still, hold breath to aiming"),			//with pressing SHIFT
	
	Crouch_State UMETA(DisplayName			= "Crouch"),					//after pressing C or when pressing CTRL
	//CrouchAim_State UMETA(DisplayName		= "Crouch with aiming"),		//after pressing C or when pressing CTRL
	Prone_State UMETA(DisplayName			= "Prone"),						//after pressing z
	//ProneAim_State UMETA(DisplayName		= "Prone with aiming"),			//after pressing Z
	
	Walk_State UMETA(DisplayName			= "Walk"),						//after pressing X
	//WalkAim_State UMETA(DisplayName			= "Walk with aiming"),			//after pressing X
	Run_State UMETA(DisplayName				= "Run"),						//moving default

	Flight_State UMETA(DisplayName			= "Flight"),					//jump or fall
	Sprint_State UMETA(DisplayName			= "Sprint"),					//with pressing SHIFT + FORWARD, this is a leap forward
	ThrowToSide_State UMETA(DisplayName		= "Throw to side"),				//with pressing SHIFT + LEFT or Right, throw to the side to deviate
	Roll_State UMETA(DisplayName			= "Roll forward")				//with pressing SHIFT + FORWARD + CTRL, roll forward, only after the sprint, 
																			//SHIFT hold for at least 2 seconds before pressing the button CTRL, 
																			//CTRL continue for no more than 2 seconds when pressing the entire combination

	//TODO: need enable
	//Jump_State UMETA(DisplayName = "Jump")
	//Fall_State UMETA(DisplayName = "Fall")
};

USTRUCT(BlueprintType)
struct FMovementSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement speed")
		float StandStill_Speed = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement speed")
		float Prone_Speed = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement speed")
		float Crouch_Speed = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement speed")
		float Walk_Speed = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement speed")
		float Run_Speed = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement speed")
		float Sprint_Speed = 800.0f;
};

//UENUM(BlueprintType)
//enum class EDirectionMovingState : uint8
//{
//	StandStill_State UMETA(DisplayName	= "Stand still"),
//	Forward_State UMETA(DisplayName		= "Forward"),
//	//Back_State UMETA(DisplayName		= "Back"),
//	//Left_State UMETA(DisplayName		= "Left"),
//	//Right_State UMETA(DisplayName		= "Right")
//	Other_State UMETA(DisplayName		= "Movement in other directions")
//};

UENUM(BlueprintType)
enum class EPositionState : uint8
{
	Stand_State UMETA(DisplayName			= "Stand"),
	Crouch_State UMETA(DisplayName			= "Crouch"),
	LyingDown_State UMETA(DisplayName		= "Lying down"),
	Flight_State UMETA(DisplayName			= "in flight"),
	OtherMovement_State UMETA(DisplayName	= "Other movement")
};

UENUM(BlueprintType)
enum class EItemType : uint8
{
	None_Type UMETA(DisplayName = "NONE"),
	//Weapon_Type UMETA(DisplayName = "Weapon"),
	//WeaponComponent_Type UMETA(DisplayName = "WeaponComponent"),
	FirstAid_Type UMETA(DisplayName = "First aid"),
	Ammo_Type UMETA(DisplayName = "Ammo")

	//knife
};

UENUM(BlueprintType)
enum class EWeaponComponentType : uint8
{
	None_Type UMETA(DisplayName = "NONE")
	//ModWeapon_OpticCH
	//ModWeapon_LaserCH
	//ModWeapon_GunBarrel (Barrel amplifier)
	//ModWeapon_Muffler
	//ModWeapon_Magazin
};

UENUM(BlueprintType)
enum class EGranadeType : uint8
{
	None_Type UMETA(DisplayName = "NONE")
	//grenade_explosive
	//grenade_smoke
	//grenade_flash
};

UENUM(BlueprintType)
enum class EAdditionalEquipmentType : uint8
{
	None_Type UMETA(DisplayName = "NONE")
	//trap (claymore)
	//remote bomb (with C4)
};

UENUM(BlueprintType)
enum class ESizeBox : uint8
{
	None_Box UMETA(DisplayName		= "NONE"),
	Big_Box UMETA(DisplayName		= "Big"),
	Medium_Box UMETA(DisplayName	= "Medium"),
	Small_Box UMETA(DisplayName		= "Small")
};

UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	WithoutWeapon_State UMETA(DisplayName	= "Without weapon"),
	//ReadyToFire_State UMETA(DisplayName		= "No action, ready to fire"),		//there is at least one bullet
	//ReadyToReload_State UMETA(DisplayName	= "No action, ready to reload"),	//no bullets
	ReadyToUse_State UMETA(DisplayName		= "No action, ready to fire or reload"),

	Fire_State UMETA(DisplayName			= "Fire"),
	//FireWithAiming_State UMETA(DisplayName	= "FireWithAiming"),
	Reload_State UMETA(DisplayName			= "Reload state"),
	Switch_State UMETA(DisplayName			= "Switch state")
	//change fire mode
	//melee
};

////bullet visual design
//USTRUCT(BlueprintType)
//struct FBulletInfo : public FTableRowBase
//{
//	GENERATED_BODY()
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bullet")
//		TSubclassOf<class AActor> ShellBullets = nullptr;	//bullet, ����
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bullet")
//		EWeaponType WeaponType = EWeaponType::None_Type;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bullet")
//		UStaticMesh* ClipDropMesh;							//bullet sleeve, ������
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bullet")
//		UStaticMesh* BulletMesh;							
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bullet")
//		UDecalComponent* DecalOnHit = nullptr;		//���� �� ���� �� ������� ��� ���������, ���� ���� �� ����, � ������ �� ������ ���������
//
//};

// description of the characteristics of the bullet or grenade
USTRUCT(BlueprintType)
struct FProjectileInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TSubclassOf<class ASEPA_ProjectileBase> ProjectileElement = nullptr;
	
	//======================================================================================
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	//	FBulletInfo BulletInfo;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bullet")
	//	TSubclassOf<class AActor> ShellBullets = nullptr;	//bullet, ����
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bullet")
	//	EWeaponType WeaponType = EWeaponType::None_Type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bullet")
		UStaticMesh* ClipDropMesh;							//bullet sleeve, ������
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bullet")
	//	UStaticMesh* BulletMesh;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bullet")
	//	UDecalComponent* DecalOnHit = nullptr;		//���� �� ���� �� ������� ��� ���������, ���� ���� �� ����, � ������ �� ������ ���������
	//======================================================================================
	
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	//	UStaticMesh* ProjectileObject = nullptr;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	//	FTransform ProjectileObjectOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		UParticleSystem* ProjectileTrailFx = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		FTransform ProjectileTrailFxOffset = FTransform();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileDamage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileLifeTime = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileBeginSpeed = 2000.0f;	//ProjectileInitSpeed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileMaxSpeed = 3000.0f;

	//material to decal on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	//Sound when hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		USoundBase* HitSound = nullptr;
	//fx when hit check by surface
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	//	TSubclassOf<UTPS_StateEffect> Effect = nullptr;

	//Explode
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		UParticleSystem* ExplodeFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		USoundBase* ExplodeSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ProjectileMinRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ExplodeMaxDamage = 40.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ExplodeFalloffCoef = 1.0f;

};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Aim_StateDispersionAimMax = 2.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Aim_StateDispersionAimMin = 0.3f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Aim_StateDispersionAimRecoil = 1.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Aim_StateDispersionReduction = .3f;

		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float AimWalk_StateDispersionAimMax = 1.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float AimWalk_StateDispersionAimMin = 0.1f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float AimWalk_StateDispersionAimRecoil = 1.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float AimWalk_StateDispersionReduction = 0.4f;

		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Walk_StateDispersionAimMax = 5.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Walk_StateDispersionAimMin = 1.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Walk_StateDispersionAimRecoil = 1.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Walk_StateDispersionReduction = 0.2f;

		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Run_StateDispersionAimMax = 10.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Run_StateDispersionAimMin = 4.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Run_StateDispersionAimRecoil = 1.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		//	float Run_StateDispersionReduction = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		EWeaponType WeaponType = EWeaponType::Pistol_Type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Reduction = 0.3f;
};

//MyComment: settings for changing Dispersion in different state
USTRUCT(BlueprintType)
struct FChangingDispersionSettings : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
		EMovementState MovementState = EMovementState::StandStill_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
		EPositionState PositionState = EPositionState::Stand_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
		bool IsAim = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
		bool IsHoldBreath = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
		float RateOfChange_AimMax = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
		float RateOfChange_AimMin = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
		float RateOfChange_AimRecoil = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
		float RateOfChange_Reduction = 0;
};

////MyComment: it's only for return data in weapon base
//USTRUCT(BlueprintType)
//struct FReturnStuct_DispersionSettings
//{
//	GENERATED_BODY()
//
//	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ChangingDispersion")
//	//	EMovementState MovementState = EMovementState::StandStill_State;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
//		float RateOfChange_AimMax = 0;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
//		float RateOfChange_AimMin = 0;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
//		float RateOfChange_AimRecoil = 0;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Changing Dispersion")
//		float RateOfChange_Reduction = 0;
//};

//MyComment: settings for changing character abilities 
USTRUCT(BlueprintType)
struct FHealthLevelSettings : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
		int32 HealthLevel = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
		float Health_Max = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
		float Health_Min = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
		float PercentageChange_Accuracy = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
		float PercentageChange_MovementSpeed = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
		float NormalWeight_ForRunSprint = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
		float MaxWeight_ForRunSprint = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
		float MaxTime_HoldBreath = 5.f;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
	//	float MaxTime_CooldownHoldBreath = 5.f;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
	//	float MaxTime_Sprint = 10.f;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Percentage change")
	//	float MaxTime_CooldownSprint = 5.f;
};

USTRUCT(BlueprintType)
struct FHealthRegenerationSettings : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina change")
		EMovementState MovementState = EMovementState::StandStill_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina change")
		EPositionState PositionState = EPositionState::Stand_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina change")
		float CoefficientOfRegeneration = 0.f;
};

USTRUCT(BlueprintType)
struct FProtectionLevelSettings : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Protection change")
		int32 ProtectionLevel = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Protection change")
		float Protection_Max = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Protection change")
		float Protection_Min = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Protection change")
		float CoefficientOfDamage = 0.f;
};

USTRUCT(BlueprintType)
struct FStaminaSettings : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina change")
		EMovementState MovementState = EMovementState::StandStill_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina change")
		EPositionState PositionState = EPositionState::Stand_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina change")
		bool isRecovery = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina change")
		bool isPercent = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina change")
		float CoefficientOfChange = 0.f;

};

//MyComment: description of the characteristics of the weapon, but the animation will need to be separated
USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	//weapon settings
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		EWeaponType WeaponType = EWeaponType::None_Type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		TSubclassOf<class ASEPA_WeaponBase> WeaponClass = nullptr;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	//	int32 NumberOfBullets = 20;		//number of bullets remaining, instead Round
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* EffectFire = nullptr;

	//weapon characteristics
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float ReloadTimer = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		int32 MaxNumberOfBullets = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		int32 NumberProjectileByShot = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		bool HasAutoFireMode = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float WeaponWeight = 5.0f;

	//fire characteristics
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		FWeaponDispersion DispersionWeapon;

	//if null use trace logic (TSubclassOf<class AProjectileDefault> Projectile = nullptr), use projectile setting damage, FX and other for trace logic
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		FProjectileInfo ProjectileSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float ShotRange = 2000.0f;		//DistacneTrace

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
	//	FAnimationWeaponInfo AnimWeaponInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		UAnimMontage* AnimCharacter_Fire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		UAnimMontage* AnimCharacter_Reload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		UAnimMontage* AnimCharacter_SwitchWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		float SwitchWeapon_TimePutInInventory = 1.0f;		//default 1 sec.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		float SwitchWeapon_TimeTakeFromInventory = 1.0f;	//default 1 sec.
		
};

////MyComment: for UI
//USTRUCT(BlueprintType)
//struct FWeaponIcon : public FTableRowBase
//{
//	GENERATED_BODY()
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Icon")
//		EWeaponType WeaponType = EWeaponType::None_Type;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Icon")
//		UTexture2D* WeaponIcon = nullptr;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Icon")
//		UTexture2D* AmmoIcon = nullptr;
//
//};

//MyComment: Attention, description of items to be picked up or dropped in boxes, but not weapons

USTRUCT(BlueprintType)
struct FWeaponKit
{
	GENERATED_BODY()

	//1 - FirstWeapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponKit")
		//EWeaponType FirstWeapon = EWeaponType::None_Type;
		EWeaponType s1 = EWeaponType::None_Type;
	//2 - SecondWeapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponKit")
		//EWeaponType SecondWeapon = EWeaponType::None_Type;
		EWeaponType s2 = EWeaponType::Pistol_Type;

	//3 - Knife
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSupplyKit")
		//EWeaponType Knife = EWeaponType::None_Type;
		EWeaponType s3 = EWeaponType::None_Type;

	//4 - Grenade
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSupplyKit")
		//EWeaponType Grenade = EWeaponType::None_Type;
		EWeaponType s4 = EWeaponType::None_Type;
	//5 - AddEquipment
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSupplyKit")
		//EWeaponType AddEquipment = EWeaponType::None_Type;
		EWeaponType s5 = EWeaponType::None_Type;

	//6 - CountOfFirstAid_big
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FirstAid big")
		int32 s6 = 0;
	//7 - CountOfFirstAid_middle
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FirstAid maddle")
		int32 s7 = 0;
	//8 - CountOfFirstAid_small
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FirstAid small")
		int32 s8 = 0;

	//9 - CountOfProtection
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Protection")
		int32 s9 = 0;
};

USTRUCT(BlueprintType)
struct FItemInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		EItemType ItemType = EItemType::None_Type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		EWeaponType WeaponType = EWeaponType::None_Type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		ESizeBox SizeBox = ESizeBox::Big_Box;
	//NumberOfUnits always = 1
	 
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	//	int32 NumberOfUnits = 0;		//NumberOfBullets
};

//settings for volume in boxes
USTRUCT(BlueprintType)
struct FVolumeInBoxes : public FTableRowBase
{
	GENERATED_BODY()

	//we can use FItemInfo !!!================================================================
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boxes")
		EItemType ItemType = EItemType::None_Type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boxes")
		EWeaponType WeaponType = EWeaponType::None_Type;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NumberOfBullets")
	//	int32 FullSet = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boxes")
		ESizeBox SizeBox = ESizeBox::Big_Box;
	//================================================================

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boxes")
		int32 InBox = 0;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NumberOfBullets")
	//	int32 PercentForBig_Box = 0;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NumberOfBullets")
	//	int32 PercentForMedium_Box = 0;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NumberOfBullets")
	//	int32 PercentForSmall_Box = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UStaticMesh* BoxMesh;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	//	TSubclassOf<class ASEPA_WeaponBase> ItemClass = nullptr;
};

////MyComment: for inventory subsystem or drop
//USTRUCT(BlueprintType)
//struct FWeaponSlot
//{
//	GENERATED_BODY()
//
//	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
//	//	FName NameItem;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
//		EWeaponType WeaponType = EWeaponType::None_Type;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
//		int32 NumberOfUnits = 0;		//number of bullets remaining
//
//	bool operator==(FWeaponSlot const& other) const
//	{
//		return WeaponType == other.WeaponType;
//	}
//};

//MyComment: for inventory subsystem 
USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		EWeaponType WeaponType = EWeaponType::None_Type;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	//	ESizeBox SizeBox = ESizeBox::Big_Box;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 NumberOfUnits = 0;		//number of bullets remaining
};

//MyComment: for inventory subsystem 
USTRUCT(BlueprintType)
struct FFirstAidSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FirstAidSlot")
		ESizeBox SizeBox = ESizeBox::Big_Box;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FirstAidSlot")
		int32 NumberOfUnits = 0;		//number of boxes remaining
};

//MyComment: for inventory subsystem 
USTRUCT(BlueprintType)
struct FNumberOfSeatsForAmmo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Seat ratio")
		EWeaponType WeaponType = EWeaponType::None_Type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Seat ratio")
		float NumberOfUnits = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Seat ratio")
		float PlaceWeight = 0.1f;
};

USTRUCT(BlueprintType)
struct FLevelIcon: public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Levels")
		FName NameLevel;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Levels")
		UTexture2D* LevelIcon = nullptr;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Levels")
		//ULevel* refLevel = nullptr;
		TAssetPtr<UWorld> refLevel = nullptr;
	
};


UCLASS()
class ALPHA_172_API USEPA_Types : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

//public:
//	USEPA_Types();

};