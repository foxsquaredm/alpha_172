// Fill out your copyright notice in the Description page of Project Settings.


#include "TestHealthZone.h"
#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "Alpha_172/CharacterClass/SEPA_Character.h"
#include "Alpha_172/GameClass/SEPA_GameMode.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATestHealthZone::ATestHealthZone()
{
	OverlapComp = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComp"));
	OverlapComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	OverlapComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	OverlapComp->SetBoxExtent(FVector(200.0f));
	RootComponent = OverlapComp;

	OverlapComp->SetHiddenInGame(false);

	OverlapComp->OnComponentBeginOverlap.AddDynamic(this, &ATestHealthZone::HandleOverlap);

	DecalComp = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComp"));
	DecalComp->DecalSize = FVector(200.0f, 200.0f, 200.0f);
	DecalComp->SetupAttachment(RootComponent);
}

void ATestHealthZone::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ASEPA_Character* MyPawn = Cast<ASEPA_Character>(OtherActor);
	if (MyPawn == nullptr)
	{
		return;
	}

	//if (MyPawn->bIsCarryingObjective)
	//{
	//	ASEPA_GameMode* GM = Cast<ASEPA_GameMode>(GetWorld()->GetAuthGameMode());
	//	if (GM)
	//	{
	//		GM->CompleteMission(MyPawn, true);
	//	}
	//}
	//else
	//{
		UGameplayStatics::PlaySound2D(this, ObjectiveMissingSound);
	//}

	UE_LOG(LogTemp, Log, TEXT("Overlapped with extraction zone!"));
}
