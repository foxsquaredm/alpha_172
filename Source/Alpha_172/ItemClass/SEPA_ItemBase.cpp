// Fill out your copyright notice in the Description page of Project Settings.


#include "SEPA_ItemBase.h"

// Sets default values
ASEPA_ItemBase::ASEPA_ItemBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASEPA_ItemBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASEPA_ItemBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

