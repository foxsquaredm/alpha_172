// Fill out your copyright notice in the Description page of Project Settings.


#include "Alpha_172/WeaponClass/SEPA_WeaponBase.h"
#include "Alpha_172/GameClass/SEPA_GameInstance.h"
//#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ASEPA_WeaponBase::ASEPA_WeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	//StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	//StaticMeshWeapon->SetGenerateOverlapEvents(false);
	//StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	//StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	//InventoryComponent = CreateDefaultSubobject<USEPA_InventoryComponent>("InventoryComponent");
}


// Called when the game starts
void ASEPA_WeaponBase::BeginPlay()
{
	Super::BeginPlay();

	//WeaponInit();
}


// Called every frame
void ASEPA_WeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority())
	{
		DispersionTick(DeltaTime);
	}
}


void ASEPA_WeaponBase::DispersionTick(float DeltaTime)
{
	//if (CurrentWeaponState != EWeaponState::Reload_State)
	//{
	//	if (CurrentWeaponState != EWeaponState::Fire_State)
	//	{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		//}

		if (CurrentDispersion < CurrentDispersionMin)
		{

			CurrentDispersion = CurrentDispersionMin;

		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	//}
	////if (ShowDebug)
	////	UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void ASEPA_WeaponBase::InitWeapon()
{
	//CurrentWeaponState = EWeaponState::ReadyToUse_State;
	//WeaponInfo = InventoryComponent->GetWeaponInfo_BySlotNumber(SlotNumber);

	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	//if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	//{
	//	StaticMeshWeapon->DestroyComponent();
	//}

	//UpdateStateWeapon_OnServer(EMovementState::Run_State);
	UpdateDispersion_OnServer(EMovementState::Run_State);
}

void ASEPA_WeaponBase::SetWeaponInfo(FWeaponInfo NewWeaponInfo)
{
	WeaponInfo = NewWeaponInfo;
	FireTimer = WeaponInfo.RateOfFire;
}

//int32 ASEPA_WeaponBase::GetNumberOfAmmoRemaining(EWeaponType WeaponType)
//{
//	int32 Result = 0;
//	if (GetOwner())
//	{
////		UE_LOG(LogTemp, Error, TEXT("AWeaponDefault::CheckCanWeaponReload - OwnerName = %s"), *GetOwner()->GetName());
//		USEPA_InventoryComponent* MyInventory = Cast<USEPA_InventoryComponent>(GetOwner()->GetComponentByClass(USEPA_InventoryComponent::StaticClass()));
//		if (MyInventory)
//		{
//			Result = MyInventory->GetRemainderAmmo_ByWeaponType(WeaponType);		
//		}
//	}
//
//	return Result;
//}

FProjectileInfo ASEPA_WeaponBase::GetProjectile()
{
	return WeaponInfo.ProjectileSetting;
}

//FBulletInfo ASEPA_WeaponBase::GetBullet()
//{
//	return ProjectileInfo.BulletInfo;
//}

void ASEPA_WeaponBase::Fire_Start()
{
	Fire_Execution();
	if (WeaponInfo.HasAutoFireMode)
	{
		GetWorld()->GetTimerManager().SetTimer(Timer_Fire, this, &ASEPA_WeaponBase::Fire_Execution, FireTimer, true);
	}
}

void ASEPA_WeaponBase::Fire_Finish()
{
	GetWorld()->GetTimerManager().ClearTimer(Timer_Fire);
}

void ASEPA_WeaponBase::Fire_Execution()
{
	if (NumberOfBullets > 0.f)
	{
		//CurrentWeaponState = EWeaponState::Fire_State;
		UAnimMontage* AnimToFire = nullptr;				//AnimToPlay
		AnimToFire = WeaponInfo.AnimCharacter_Fire;
		//	//AnimWeaponStart_Multicast(AnimToFire);

		//	//WeaponInfo.Round --;
		ChangeDispersionByShot();
		//	OnWeaponFireStart.Broadcast(AnimToFire);
		//
		//	//FXWeaponFire_Multicast(WeaponInfo.EffectFireWeapon, WeaponInfo.SoundFireWeapon);


		//int8 NumberProjectile = GetNumberProjectileByShot();
		int8 NumberProjectile = WeaponInfo.NumberProjectileByShot;

		if (ShootLocation)
		{
			FVector SpawnLocation = ShootLocation->GetComponentLocation();
			FRotator SpawnRotation = ShootLocation->GetComponentRotation();
			ProjectileInfo = GetProjectile();
			//FBulletInfo BulletInfo;
			//BulletInfo = GetBullet();


			FVector EndLocation;
			for (int8 i = 0; i < NumberProjectile; i++)		//if Shotgun, when i > 1
			{
				//EndLocation = GetFireEndLocation();

				//FVector Dir = EndLocation - SpawnLocation;
				//Dir.Normalize();
				////FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				////SpawnRotation = myMatrix.Rotator();

				if (ProjectileInfo.ProjectileElement)
				{
					//Projectile Init ballistic fire

					FActorSpawnParameters SpawnParams;
					SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
					SpawnParams.Owner = GetOwner();
					SpawnParams.Instigator = GetInstigator();

					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + SpawnRotation.Vector() * 1000, FColor::Red, true);
					//DrawDebugLine(GetWorld(), SpawnLocation, EndLocation, FColor::Red, true);

					ASEPA_ProjectileBase* myProjectile = Cast<ASEPA_ProjectileBase>(GetWorld()->SpawnActor(ProjectileInfo.ProjectileElement, &SpawnLocation, &SpawnRotation, SpawnParams));
					if (myProjectile)
					{
						myProjectile->InitProjectile(WeaponInfo.ProjectileSetting);


						//drop shell bullets
						//AActor* curShellBullets = GetWorld()->SpawnActor<AActor>(ProjectileInfo.ShellBullets, GetActorLocation(), FRotator::ZeroRotator);
						//if (curShellBullets)
						//{
						//	UPrimitiveComponent* PrimitivElement = Cast<UPrimitiveComponent>(curShellBullets->GetComponentByClass(UPrimitiveComponent::StaticClass()));
						//	if (PrimitivElement)
						//	{
						//		PrimitivElement->AddImpulse(GetActorRightVector() * 2.f);
						//	}
						//}

						//				UE_LOG(LogTemp, Warning, TEXT("Droped shell bullets.================"));
					}
					//}
				}
			}
		}
	}
}

//void ASEPA_WeaponBase::Reload_Start()
//{
//	if (GetNumberOfAmmoRemaining(WeaponInfo.WeaponType) > 0)
//	{
//		CurrentWeaponState = EWeaponState::Reload_State;
//
//		//int32 NumberOfBullets_Reload = 0.f;
//		int32 MaxNumberOfBullets = WeaponInfo.MaxNumberOfBullets;
//
//		int32 NumberOfBullets_Reload = GetNumberOfBullets_ToReload(MaxNumberOfBullets);
//		ReloadTimer = GetTime_ToReload(WeaponInfo.WeaponType, NumberOfBullets_Reload, MaxNumberOfBullets);
//		//FVector SpawnLocation = FVector(0);
//		
//		NumberOfBullets = 0.0f;
//
//		FVector SpawnLocation = FVector(0);
//		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponInfo.SoundReload, SpawnLocation);
//
//		UAnimMontage* AnimWeaponToPlay = nullptr;
//		AnimWeaponToPlay = WeaponInfo.AnimCharacter_Reload;
//
//		if (WeaponInfo.AnimCharacter_Reload
//			&& SkeletalMeshWeapon
//			&& SkeletalMeshWeapon->GetAnimInstance())
//		{
//			//if (AnimWeaponToPlay
//			//	&& SkeletalMeshWeapon
//			//	&& SkeletalMeshWeapon->GetAnimInstance())
//			//{
//			//	SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimWeaponToPlay);
//			//}
//			PlayOurAnimation(AnimWeaponToPlay);
//		}
//	}
//}

//void ASEPA_WeaponBase::Reload_Finish()
//{
//	CurrentWeaponState = EWeaponState::ReadyToUse_State;
//	ReloadTimer = 0.f;
//
//	int32 MaxNumberOfBullets = WeaponInfo.MaxNumberOfBullets;
//	NumberOfBullets = GetNumberOfBullets_ToReload(MaxNumberOfBullets);
//
//	InventoryComponent->ReduceAfterReload_Ammo(WeaponInfo.WeaponType, NumberOfBullets);
//
//	//OnWeaponReloadFinish.Broadcast(true, NumberOfBullets_Reload);
//}

//void ASEPA_WeaponBase::ExecutionAborted_Reload()
//{
//	StopOurAnimation();
//	CurrentWeaponState = EWeaponState::ReadyToUse_State;
//
//	//OnWeaponReloadFini.Broadcast(false, 0);
//	////DropClipFlag = false;
//}

//void ASEPA_WeaponBase::SwitchWeapon_Start(int32 NewSlotNumber)
//{
//	CurrentWeaponState = EWeaponState::Switch_State;
//	////SwitchWeapon(NextWeapon);
//	//FWeaponInfo Old_WeaponInfo = WeaponInfo;
//	//FWeaponInfo WeaponInfo = InventoryComponent->GetWeaponInfo_FromWeaponSlot(WeaponInfo.WeaponType, NextWeapon);
//
//	//SwitchWeaponTimer = Old_WeaponInfo.SwitchWeapon_TimePutInInventory + WeaponInfo.SwitchWeapon_TimeTakeFromInventory;
//
//
//	UAnimMontage* AnimWeaponToPlay = nullptr;
//	//MyComment: this animation for put weapon in inventory
//	AnimWeaponToPlay = WeaponInfo.AnimCharacter_SwitchWeapon;
//
//	if (WeaponInfo.AnimCharacter_SwitchWeapon
//		&& SkeletalMeshWeapon
//		&& SkeletalMeshWeapon->GetAnimInstance())
//	{
//		PlayOurAnimation(AnimWeaponToPlay);
//	}
//
//	//MyComment: this animation for take weapon from inventory
//	FWeaponInfo NewWeaponInfo = InventoryComponent->GetWeaponInfo_BySlotNumber(NewSlotNumber);
//	AnimWeaponToPlay = WeaponInfo.AnimCharacter_SwitchWeapon;	//!!! revers
//
//	if (WeaponInfo.AnimCharacter_SwitchWeapon
//		&& SkeletalMeshWeapon
//		&& SkeletalMeshWeapon->GetAnimInstance())
//	{
//		PlayOurAnimation(AnimWeaponToPlay);
//	}
//
//}

////MyComment: it's time for put weapon in inventory. For switch or put in inventory
//float ASEPA_WeaponBase::GetTimePut(int32 SlotNumber)
//{
//	float result = 0.f;
//	FWeaponInfo Old_WeaponInfo = WeaponInfo;
//	if (Old_WeaponInfo.WeaponType != EWeaponType::None_Type)
//	{
//		result = Old_WeaponInfo.SwitchWeapon_TimePutInInventory;
//	}
//
//	return result;
//}

////MyComment: it's time for take weapon from inventory. For take from inventory 
//float ASEPA_WeaponBase::GetTimeTake(int32 SlotNumber)
//{
//	float result = 0.f;
//	WeaponInfo = InventoryComponent->GetWeaponInfo_BySlotNumber(SlotNumber);
//	if (WeaponInfo.WeaponType != EWeaponType::None_Type)
//	{
//		result = WeaponInfo.SwitchWeapon_TimeTakeFromInventory;
//	}
//
//	return result;
//}

//void ASEPA_WeaponBase::SwitchWeapon_Finish()
//{
//	//CurrentWeaponState = EWeaponState::ReadyToUse_State;
//	InitWeapon();
//}

//void ASEPA_WeaponBase::ExecutionAborted_SwitchWeapon()
//{
//	StopOurAnimation();
//	CurrentWeaponState = EWeaponState::ReadyToUse_State;
//}


//void ASEPA_WeaponBase::PlayOurAnimation(UAnimMontage* AnimWeaponToPlay)
//{
//	if (AnimWeaponToPlay
//		&& SkeletalMeshWeapon
//		&& SkeletalMeshWeapon->GetAnimInstance())
//	{
//		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimWeaponToPlay);
//	}
//}

//void ASEPA_WeaponBase::StopOurAnimation()
//{
//	CurrentWeaponState = EWeaponState::ReadyToUse_State;
//	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
//	{
//		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
//	}
//}


//int32 ASEPA_WeaponBase::GetNumberOfBullets_ToReload(int32 MaxNumberOfBullets)
//{
//	int32 NumberOfBullets_Reload = 0;
//
//	int32 SlotNumber = InventoryComponent->GetItemSlotNumber_Ammo(WeaponInfo.WeaponType);
//	int32 RemainderNumberOfUnits = InventoryComponent->GetRemainderAmmo_BySlotNumber(SlotNumber--);
//
//	if (RemainderNumberOfUnits >= (MaxNumberOfBullets - NumberOfBullets))
//	{
//		NumberOfBullets_Reload = MaxNumberOfBullets - NumberOfBullets;
//	}
//	else
//	{
//		NumberOfBullets_Reload = RemainderNumberOfUnits;
//	}
//
//	return NumberOfBullets_Reload;
//}

//float ASEPA_WeaponBase::GetTime_ToReload(EWeaponType WeaponType, int32 NumberOfBullets_Reload, int32 MaxNumberOfBullets)
//{
//	float TimeToReload = 0.0f;
//	float ReloadTime = WeaponInfo.ReloadTimer;
//
//	if (WeaponType == EWeaponType::ShotGun_Type)
//	{
//		//if shotgun, when:
//		TimeToReload = (ReloadTime / MaxNumberOfBullets) * NumberOfBullets_Reload;
//	}
//	else
//	{
//		//if other weapon, when there is time to pull out and put magazin:
//		TimeToReload = ReloadTime + (0.1 * NumberOfBullets_Reload);
//	}
//
//	return TimeToReload;
//}

void ASEPA_WeaponBase::UpdateDispersion_OnServer_Implementation(EMovementState NewMovementState)
{
	//switch (NewMovementState)
	//{
	//case EMovementState::Aim_State:

	//	CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
	//	CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
	//	CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
	//	CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
	//	break;
	//	//case EMovementState::AimWalk_State:

	//	//	CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
	//	//	CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
	//	//	CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
	//	//	CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
	//	//	break;
	//	//case EMovementState::Walk_State:

	//	//	CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
	//	//	CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
	//	//	CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
	//	//	CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
	//	//	break;
	//case EMovementState::Run_State:

	//	CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
	//	CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
	//	CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
	//	CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
	//	break;
	//default:
	//	break;
	//}

	FWeaponDispersion DispersionWeapon;
	DispersionWeapon = WeaponInfo.DispersionWeapon;
	FChangingDispersionSettings StructureDispersionSettings;
	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		StructureDispersionSettings = myGameInst->GetRateOfChange(NewMovementState);
	}

	CurrentDispersionMax		= DispersionWeapon.AimMax * StructureDispersionSettings.RateOfChange_AimMax;
	CurrentDispersionMin		= DispersionWeapon.AimMin * StructureDispersionSettings.RateOfChange_AimMin;
	CurrentDispersionRecoil		= DispersionWeapon.AimRecoil * StructureDispersionSettings.RateOfChange_AimRecoil;
	CurrentDispersionReduction	= DispersionWeapon.Reduction * StructureDispersionSettings.RateOfChange_Reduction;
}

void ASEPA_WeaponBase::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float ASEPA_WeaponBase::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector ASEPA_WeaponBase::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

void ASEPA_WeaponBase::InitDropMesh_OnServer_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	if (DropMesh)
	{
		FTransform Transform;

		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());

		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());

		ShellDropFire_Multicast(DropMesh, Transform, DropImpulseDirection, LifeTimeMesh, ImpulseRandomDispersion, PowerImpulse, CustomMass, LocalDir);

	}
}

void ASEPA_WeaponBase::ShellDropFire_Multicast_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir)
{

	FActorSpawnParameters Param;
	Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Param.Owner = this;

	//AStaticMeshActor* NewActor = nullptr;
	//NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Offset, Param);

//if (NewActor && NewActor->GetStaticMeshComponent())
//{
//	NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
//	NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

//	//set parameter for new actor
//	NewActor->SetActorTickEnabled(false);
//	NewActor->InitialLifeSpan = LifeTimeMesh;

//	NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
//	NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
//	NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

//	NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
//	NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
//	NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
//	NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
//	NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
//	NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

	if (CustomMass > 0.0f)
	{
		//	NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
	}

	if (!DropImpulseDirection.IsNearlyZero())
	{
		FVector FinalDir;
		LocalDir = LocalDir + (DropImpulseDirection * 1000.0f);

		if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
			FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);
			FinalDir.GetSafeNormal(0.0001f);

		//		NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
	}
	//}
}

FVector ASEPA_WeaponBase::GetFireEndLocation() const
{
	FVector EndLocation = FVector(0.f);
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponInfo.ShotRange, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponInfo.ShotRange, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}


	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}

	return EndLocation;
}

//void ASEPA_WeaponBase::SwitchWeapon(bool NextWeapon)
//{
//	FWeaponInfo Old_WeaponInfo = WeaponInfo;
//	FWeaponInfo New_WeaponInfo = InventoryComponent->GetWeaponInfo_FromWeaponSlot(WeaponInfo.WeaponType, NextWeapon);
//
//	SwitchWeaponTimer = Old_WeaponInfo.SwitchWeapon_TimePutInInventory + New_WeaponInfo.SwitchWeapon_TimeTakeFromInventory;
//
//	//InitWeapon();
//	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
//	{
//		SkeletalMeshWeapon->DestroyComponent(true);
//	}
//
//	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
//	{
//		StaticMeshWeapon->DestroyComponent();
//	}
//
//	//UpdateStateWeapon_OnServer(EMovementState::Run_State);
//	UpdateDispersion(EMovementState::Run_State);
//}

//int8 ASEPA_WeaponBase::GetNumberProjectileByShot() const
//{
//	return WeaponInfo.NumberProjectileByShot;
//}




void ASEPA_WeaponBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

//	DOREPLIFETIME(AWeaponDefault, WeaponInfo);
//	DOREPLIFETIME(AWeaponDefault, WeaponReloading);
	DOREPLIFETIME(ASEPA_WeaponBase, ShootEndLocation);

}
