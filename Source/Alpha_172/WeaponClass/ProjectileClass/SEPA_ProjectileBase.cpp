// Copyright Epic Games, Inc. All Rights Reserved.

#include "Alpha_172/WeaponClass/ProjectileClass/SEPA_ProjectileBase.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"

ASEPA_ProjectileBase::ASEPA_ProjectileBase()
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(2.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ASEPA_ProjectileBase::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	BulletMesh->SetGenerateOverlapEvents(false);
	BulletMesh->SetCollisionProfileName(TEXT("NoCollision"));
	BulletMesh->SetupAttachment(RootComponent);

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
}

//// Called every frame
//void ASEPA_ProjectileBase::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//
//}

void ASEPA_ProjectileBase::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//// Only add impulse and destroy projectile if we hit a physics
	//if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr) && OtherComp->IsSimulatingPhysics())
	//{
	//	OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());

	//	Destroy();
	//}

	//////////////////////////////////////////////////////////////////
				//Multicast trace FX
				//ToDo Projectile null Init trace fire			

				//GetWorld()->LineTraceSingleByChannel()
	/*FHitResult Hit;
	TArray<AActor*> Actors;*/

	/*			EDrawDebugTrace::Type DebugTrace;
				if (ShowDebug)
				{
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponInfo.ShotRange, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
					DebugTrace = EDrawDebugTrace::ForDuration;
				}
				else
					DebugTrace = EDrawDebugTrace::None;*/

				//UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponInfo.ShotRange,
				//	ETraceTypeQuery::TraceTypeQuery4, false, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

	//if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
	if (Hit.GetActor())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

		if (ProjectileSetting.HitDecals.Contains(mySurfacetype))
		{
			UMaterialInterface* myMaterial = ProjectileSetting.HitDecals[mySurfacetype];

			if (myMaterial && Hit.GetComponent())
			{
				UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
			}
		}
		if (ProjectileSetting.HitFXs.Contains(mySurfacetype))
		{
			UParticleSystem* myParticle = ProjectileSetting.HitFXs[mySurfacetype];
			if (myParticle)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
			}
		}

		if (ProjectileSetting.HitSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.HitSound, Hit.ImpactPoint);
		}

		//UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileSetting.Effect, mySurfacetype);

		UGameplayStatics::ApplyPointDamage(Hit.GetActor(), ProjectileSetting.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);

	}

}

void ASEPA_ProjectileBase::InitProjectile(FProjectileInfo InitParam)
{
	this->SetLifeSpan(InitParam.ProjectileLifeTime);

	//InitVelocity_Multicast(InitParam.ProjectileBeginSpeed, InitParam.ProjectileMaxSpeed);
	if (ProjectileMovement)
	{
		ProjectileMovement->Velocity = GetActorForwardVector() * InitParam.ProjectileBeginSpeed;
		ProjectileMovement->InitialSpeed = InitParam.ProjectileBeginSpeed;
		ProjectileMovement->MaxSpeed = InitParam.ProjectileMaxSpeed;
	}

	ProjectileSetting = InitParam;
}

//void ASEPA_ProjectileBase::InitVelocity_Multicast_Implementation(float InitSpeed, float MaxSpeed)
//{
//	if (BulletMovement)
//	{
//		BulletMovement->Velocity = GetActorForwardVector() * InitSpeed;
//		BulletMovement->MaxSpeed = MaxSpeed;
//		BulletMovement->InitialSpeed = InitSpeed;
//	}
//}