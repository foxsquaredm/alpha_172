// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
//#include "Alpha_172/ComponentsActor/SEPA_InventoryComponent.h"
#include "Alpha_172/GeneralClass/SEPA_Types.h"
#include "Alpha_172/WeaponClass/ProjectileClass/SEPA_ProjectileBase.h"
#include "SEPA_WeaponBase.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFire, UAnimMontage*, AnimFire);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, AnimReload);
////DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadFinish, bool, bIsSuccess, int32, RemainingAmmo);		//instead AmmoSafe
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponSwitchStart, bool, NextWeapon);
////DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponSwitchFinish, bool, bIsSuccess);

UCLASS()
class ALPHA_172_API ASEPA_WeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASEPA_WeaponBase();

	//FOnWeaponFire			OnWeaponFire;
	//FOnWeaponReloadStart	OnWeaponReloadStart;
	////FOnWeaponReloadFinish	OnWeaponReloadFinish;
	//FOnWeaponSwitchStart	OnWeaponSwitchStart;
	////FOnWeaponSwitchFinish	OnWeaponSwitchFinish;

	//EWeaponState CurrentWeaponState;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY()
		FWeaponInfo WeaponInfo;			//instead WeaponSetting
	UPROPERTY()
		FProjectileInfo ProjectileInfo;
	//UPROPERTY()
	//	FBulletInfo BulletInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SettingWeapon")
		EWeaponType WeaponType = EWeaponType::Rifle_Type;

	//TODO: when drop weapon, we have to write number of bullets remaining !!!
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SettingWeapon")
		int32 NumberOfBullets = 0.0f;
	//UPROPERTY()
	//	int32 NumberOfBullets_Reload = 0.0f;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	FTimerHandle Timer_Fire;

//private:
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
//		class USEPA_InventoryComponent* InventoryComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	//UPROPERTY()
	//	ASEPA_WeaponBase* CurrentWeapon;

//	//UFUNCTION()
//	//	ASEPA_WeaponBase* GetCurrentWeapon();

	//void FireTick(float DeltaTime);
	//void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	//void SwitchWeaponTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
		void InitWeapon();
	UFUNCTION(BlueprintCallable)
		void SetWeaponInfo(FWeaponInfo NewWeaponInfo);

	//UFUNCTION(BlueprintCallable)
	//	int32 GetNumberOfAmmoRemaining(EWeaponType WeaponType);
	//UFUNCTION(BlueprintCallable)
	//	int32 GetNumberOfBulletsRemaining();

	//void InitReload();
	FProjectileInfo GetProjectile();
	//FBulletInfo GetBullet();

	void Fire_Start();
	void Fire_Finish();
	void Fire_Execution();

	//void Reload_Start();
	//void Reload_Finish();
	//void ExecutionAborted_Reload();
	//void SwitchWeapon_Start(int32 NewSlotNumber);
	////void SwitchWeapon_Finish();
	//float GetTimePut(int32 SlotNumber);
	//float GetTimeTake(int32 SlotNumber);

	//void ExecutionAborted_SwitchWeapon();
	//void PlayOurAnimation(UAnimMontage* AnimWeaponToPlay);
	//void StopOurAnimation();


	//int32 GetNumberOfBullets_ToReload(int32 MaxNumberOfBullets);
	//float GetTime_ToReload(EWeaponType WeaponType, int32 NumberOfBulletsForReload, int32 MaxNumberOfBullets);

	UFUNCTION(Server, Reliable)
		void UpdateDispersion_OnServer(EMovementState NewMovementState);
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot)const;
 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;
	UFUNCTION(Server, Reliable)
		void InitDropMesh_OnServer(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.0f;
	UFUNCTION(NetMulticast, UnReliable)
		void ShellDropFire_Multicast(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir);


	FVector GetFireEndLocation() const;
	//int8 GetNumberProjectileByShot() const;

	//Timers
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
		float FireTimer = 0.0f;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
	//	float ReloadTimer = 0.0f;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
	//	float SwitchWeaponTimer = 0.0f;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer Debug")	//Remove !!! Debug
	//	float ReloadTime = 0.0f;

		//Dispersion
	//UPROPERTY(Replicated) //��������������� ��� ��������
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	UPROPERTY(Replicated)
		FVector ShootEndLocation = FVector(0);

	////change current weapon	in this class
	//UFUNCTION()
	//	void SwitchWeapon(bool NextWeapon);

};
