// Fill out your copyright notice in the Description page of Project Settings.


#include "SEPA_HealthComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
USEPA_HealthComponent::USEPA_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;

	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void USEPA_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	//SetHealthLevel_byHealth();
	SetTimeERD();
	SetTimeEHR();

}


// Called every frame
void USEPA_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void USEPA_HealthComponent::InitializeComponent()
{
	Super::InitializeComponent();

	SetHealthLevel_byHealth();
}

void USEPA_HealthComponent::SetTimeERD()
{
	rateTimeERD = Time_StartRecovery / 50;
}

void USEPA_HealthComponent::SetTimeEHR()
{
	rateTimeEHR = Time_TotalRecovery / 50;
}

void USEPA_HealthComponent::Execution_ReceivedDamage()
{
	if (CharacterReceivedDamage)
	{
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(Timer_ReceivedDamage);
			GetWorld()->GetTimerManager().ClearTimer(Timer_HealthRecovery);
			GetWorld()->GetTimerManager().SetTimer(Timer_ReceivedDamage, this, &USEPA_HealthComponent::Execution_ReceivedDamage, rateTimeERD, true);
		}
		CharacterReceivedDamage = false;
		TimeLeft_StartRecovery = Time_StartRecovery;
	}
	else
	{
		if (TimeLeft_StartRecovery > 0)
		{
			TimeLeft_StartRecovery -= rateTimeERD;
		}
		else
		{
			TimeLeft_StartRecovery = 0.f;
			if (GetWorld())
			{
				GetWorld()->GetTimerManager().ClearTimer(Timer_ReceivedDamage);
				GetWorld()->GetTimerManager().SetTimer(Timer_HealthRecovery, this, &USEPA_HealthComponent::Execution_HealthRecovery, rateTimeEHR, true);
			}
		}
	}
}

void USEPA_HealthComponent::Execution_HealthRecovery()
{
	float MaxLevel = GetMaxHealth_ByCurrentHealthLevel();
	//float Coeff = Get_CoefficientOfRegeneration();
	
	if (Health == MaxLevel)
	{
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(Timer_HealthRecovery);
		}
	}
	else
	{
		if ((Health + CoefficientOfRegeneration) >= MaxLevel)
		{
			Health = MaxLevel;
			OnHealthChange.Broadcast();
		}
		else
		{
			Health += CoefficientOfRegeneration;
			OnHealthChange.Broadcast();
		}
	}
}

void USEPA_HealthComponent::SetHealthLevel_byHealth()
{
	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		HealthLevel = myGameInst->GetHealthLevel_fromTabl(Health);
	}
}

bool USEPA_HealthComponent::GetIsAlive()
{
	return (Health > 0.0f);
}

float USEPA_HealthComponent::GetHealthPercent()
{
	return round(Health);
}

int USEPA_HealthComponent::GetHealthLevel()
{
	return HealthLevel;
}

int32 USEPA_HealthComponent::GetHealthLevel_fromTabl()
{
	int32 reCurrentHealthLevel = 0;
	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		reCurrentHealthLevel = myGameInst->GetHealthLevel_fromTabl(Health);
	}

	return reCurrentHealthLevel;
}

float USEPA_HealthComponent::GetMaxHealth_ByCurrentHealthLevel()
{
	float MaxHealth = 0.f;
	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		MaxHealth = myGameInst->GetMaxHealth_ByCurrentHealthLevel(HealthLevel);
	}

	return MaxHealth;
}

void USEPA_HealthComponent::Increase_HealthLevel()
{
	HealthLevel ++;
}

void USEPA_HealthComponent::Decrease_HealthLevel()
{
	HealthLevel --;
}

float USEPA_HealthComponent::Get_CoefficientOfRegeneration(EMovementState MovementState, EPositionState PositionState)
{
	float Coeff = 0.f;
	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		Coeff = myGameInst->GetCoefficientOfRegeneration(MovementState, PositionState);
	}

	return Coeff;
}

void USEPA_HealthComponent::Set_CoefficientOfRegeneration(float Value)
{
	CoefficientOfRegeneration = Value;
}

void USEPA_HealthComponent::RestoreWithFirstAid(float IncreaseHealth)
{
	Health += IncreaseHealth;
	OnHealthChange.Broadcast();
	int32 reCurrentHealthLevel = GetHealthLevel_fromTabl();

	GetWorld()->GetTimerManager().SetTimer(Timer_ReceivedDamage, this, &USEPA_HealthComponent::Execution_ReceivedDamage, rateTimeERD, true);

	if (HealthLevel != reCurrentHealthLevel)
	{
		HealthLevel = reCurrentHealthLevel;
		OnHealthLevelChange.Broadcast();
	}
}

void USEPA_HealthComponent::ChangeHealth_OnDamage(float DamageValue)
{
	if (DamageValue > 0.f)
	{
		CharacterReceivedDamage = true;
		Health -= DamageValue;
		int32 reCurrentHealthLevel = GetHealthLevel_fromTabl();

		if (Health <= 0.0f)
		{
			Health = 0.0f;
			HealthLevel = 0;
			OnHealthChange.Broadcast();
			OnHealthLevelChange.Broadcast();
			OnDead.Broadcast();
		}
		else
		{
			OnHealthChange.Broadcast();

			GetWorld()->GetTimerManager().SetTimer(Timer_ReceivedDamage, this, &USEPA_HealthComponent::Execution_ReceivedDamage, rateTimeERD, true);

			if (HealthLevel != reCurrentHealthLevel)
			{
				HealthLevel = reCurrentHealthLevel;
				OnHealthLevelChange.Broadcast();
			}
		}
	}
}




void USEPA_HealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USEPA_HealthComponent, HealthLevel);
	DOREPLIFETIME(USEPA_HealthComponent, Health);
	//DOREPLIFETIME(UTPSHealthComponent, bIsAlive);

}