// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Alpha_172/GameClass/SEPA_GameInstance.h"
#include "SEPA_HealthComponent.generated.h"


//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthLevelChange, int32, HealthLevel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHealthLevelChange);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthChange, float, CurrentHealth);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHealthChange);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ALPHA_172_API USEPA_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USEPA_HealthComponent();

	//UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	//	FOnHealthLevelChange OnHealthLevelChange;
	UPROPERTY(BlueprintAssignable, Category = "Health")
		FOnHealthLevelChange OnHealthLevelChange;
	//UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	//	FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, Category = "Health")
		FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnDead OnDead;

	virtual void InitializeComponent() override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		int32 HealthLevel = 0;
	UPROPERTY(Replicated)
		float Health = 80.0f;
	
	UPROPERTY()
		bool CharacterReceivedDamage = false;
	//UPROPERTY()
	//	float TimeToStarRestore = 2.f;
	//UPROPERTY()
	//	float CurrentTimeToStarRestore = 0.f;

	//////////////////////////////////////////////////////////////////////
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
		float Time_StartRecovery = 2.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
		float Time_TotalRecovery = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
		float CoefficientOfRegeneration = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
		float TimeLeft_StartRecovery = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
		float rateTimeERD = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
		float rateTimeEHR = 0.f;

	FTimerHandle Timer_ReceivedDamage;
	FTimerHandle Timer_HealthRecovery;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "RunTimer")
		void SetTimeERD();
	UFUNCTION(BlueprintCallable, Category = "RunTimer")
		void SetTimeEHR();
	UFUNCTION(BlueprintCallable, Category = "RunTimer")
		void Execution_ReceivedDamage();
	UFUNCTION(BlueprintCallable, Category = "RunTimer")
		void Execution_HealthRecovery();

	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetHealthLevel_byHealth();

	UFUNCTION(BlueprintCallable, Category = "Health")
		bool GetIsAlive();
	UFUNCTION(BlueprintCallable, Category = "Health")
		float GetHealthPercent();

	UFUNCTION(BlueprintCallable, Category = "Health")
		int32 GetHealthLevel();
	UFUNCTION(BlueprintCallable, Category = "Health")
		int32 GetHealthLevel_fromTabl();
	UFUNCTION(BlueprintCallable, Category = "Health")
		float GetMaxHealth_ByCurrentHealthLevel();
	UFUNCTION(BlueprintCallable, Category = "Health")
		float Get_CoefficientOfRegeneration(EMovementState MovementState, EPositionState PositionState);
	UFUNCTION(BlueprintCallable, Category = "Health")
		void Set_CoefficientOfRegeneration(float Value);
	UFUNCTION(BlueprintCallable, Category = "Health")
		void RestoreWithFirstAid(float IncreaseHealth);
	UFUNCTION(BlueprintCallable, Category = "Health")
		void ChangeHealth_OnDamage(float DamageValue);

	UFUNCTION(BlueprintCallable, Category = "Health")
		void Increase_HealthLevel();
	UFUNCTION(BlueprintCallable, Category = "Health")
		void Decrease_HealthLevel();

};
