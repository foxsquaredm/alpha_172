// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Alpha_172/GeneralClass/SEPA_Types.h"
#include "Alpha_172/GameClass/SEPA_GameInstance.h"
#include "Components/ActorComponent.h"
#include "SEPA_StaminaComponent.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStaminaChange, float, CurrentStamina);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStaminaChange);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ALPHA_172_API USEPA_StaminaComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USEPA_StaminaComponent();

	UPROPERTY(BlueprintAssignable, Category = "Stamina")
		FOnStaminaChange OnStaminaChange;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY()
		float Stamina = 100.f;
	UPROPERTY(BlueprintReadOnly)
		float CoefficientOfChangeStamina = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
		float Time_TotalRecovery = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
		float rateTimeESD = 0.f;

	FTimerHandle Timer_StaminaRecovery;

	UFUNCTION(BlueprintCallable, Category = "RunTimer")
		void SetTimeESD();
	UFUNCTION(BlueprintCallable, Category = "RunTimer")
		void Execution_StaminaRecovery();

	UPROPERTY()
		bool CharacterMakeMoreEffort = false;
	UPROPERTY()
		bool Enable_StaminaRecovery = false;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Stamina")
		float GetStaminaPercent();
	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void SetCoefficientOfChangeStamina(EMovementState MovementState, EPositionState PositionState, bool isRecovery);
	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void SetCharacterMakeMoreEffort(bool Value);
	UFUNCTION()
		void SetEnableStaminaRecovery(bool Value);
};
