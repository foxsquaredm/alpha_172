// Fill out your copyright notice in the Description page of Project Settings.


#include "SEPA_InventoryComponent.h"
#include "Alpha_172/GameClass/SEPA_GameInstance.h"

// Sets default values for this component's properties
USEPA_InventoryComponent::USEPA_InventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USEPA_InventoryComponent::BeginPlay()
{
	Super::BeginPlay();	
}

//// Called every frame
//void USEPA_InventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
//{
//	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//
//	// ...
//}


void USEPA_InventoryComponent::PickUpItem(EItemType ItemType, EWeaponType WeaponType, ESizeBox SizeBox)
{
	//!!! so far we are collecting everything that will be lying around in locations, no limits

	int32 SlotNumber = 0;
	if (ItemType == EItemType::Ammo_Type)
	{
		SlotNumber = GetItemSlotNumber_Ammo(WeaponType);
		//SlotNumber = GetItemSlotNumber(ItemType, WeaponType, ESizeBox::None_Box);
		//int32 NumberOfUnits = USEPA_Types::GetNumberOfUnits(ItemType, WeaponType, SizeBox);
		//int32 NumberOfUnits = USEPA_GameInstance::GetNumberOfUnits(ItemType, WeaponType, SizeBox);
		int32 NumberOfUnits = 0;
		USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
		if (myGameInst)
		{
			NumberOfUnits = myGameInst->GetNumberOfUnits(ItemType, WeaponType, SizeBox);
		}

		if (SlotNumber == 0)
		{
			FAmmoSlot NewAmmoSlot;
			NewAmmoSlot.WeaponType = WeaponType;
			//NewAmmoSlot.SizeBox = SizeBox;
			NewAmmoSlot.NumberOfUnits = NumberOfUnits;
			//NewAmmoSlot.NumberOfUnits++;

			SlotsAmmo.Add(NewAmmoSlot);
		}
		else
		{
			SlotsAmmo[SlotNumber--].NumberOfUnits += NumberOfUnits;
		}
	}
	else if (ItemType == EItemType::FirstAid_Type)
	{
		SlotNumber = GetItemSlotNumber_FirstAid(SizeBox);
		//SlotNumber = GetItemSlotNumber(ItemType, EWeaponType::None_Type, SizeBox);

		if (SlotNumber == 0)
		{
			FFirstAidSlot NewFirstAidSlot;
			NewFirstAidSlot.SizeBox = SizeBox;
			NewFirstAidSlot.NumberOfUnits = 1;

			SlotsFirstAid.Add(NewFirstAidSlot);
		}
		else
		{
			SlotsFirstAid[SlotNumber--].NumberOfUnits++;
		}
	}
}

void USEPA_InventoryComponent::DropItem(EItemType ItemType, EWeaponType WeaponType, ESizeBox SizeBox)
{
	ReduceByItemBox(ItemType, WeaponType, SizeBox);
}

int32 USEPA_InventoryComponent::GetRemainderAmmo_BySlotNumber(int32 SlotNumber)
{
	int32 NumberOfUnits = 0;

	NumberOfUnits = SlotsAmmo[SlotNumber].NumberOfUnits;

	return NumberOfUnits;
}

int32 USEPA_InventoryComponent::GetRemainderAmmo_ByWeaponType(EWeaponType WeaponType)
{
	int32 NumberOfUnits = 0;

	//NumberOfUnits = SlotsAmmo[SlotNumber].NumberOfUnits;
	int32 SlotNumber = 0;
	SlotNumber = GetItemSlotNumber_Ammo(WeaponType);
	NumberOfUnits = GetRemainderAmmo_BySlotNumber(SlotNumber--);

	return NumberOfUnits;
}

float USEPA_InventoryComponent::CalculateNumberOfSeatsPerBullet(EWeaponType WeaponType)
{
	float NumberOfSeats = 0;
	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		NumberOfSeats = myGameInst->GetNumberOfSeats(WeaponType);
	}

	return NumberOfSeats;
}

int32 USEPA_InventoryComponent::GetTotalNumberOfBullets()
{
	int32 TotalAmmo = 200;
	bool CapacityOfBulletSlots = false;

	int32 NumberOfUnits = 0;
	int32 CurrentNumberOfUnits = 0;
	int32 NumberOfBullets = 0;
	int32 NumberOfSeats = 0;

	for (int32 i = 0; i < SlotsAmmo.Num(); i++)
	{
		CurrentNumberOfUnits = 0;
		NumberOfBullets = 0;
		NumberOfSeats = 0;

		CurrentNumberOfUnits = SlotsAmmo[i--].NumberOfUnits;
		NumberOfSeats = CalculateNumberOfSeatsPerBullet(SlotsAmmo[i--].WeaponType);
		NumberOfUnits = NumberOfUnits + (CurrentNumberOfUnits * NumberOfSeats);
	}

	return CapacityOfBulletSlots;
}

bool USEPA_InventoryComponent::CheckForLackOfPlacesToCollectBullets(int32 NumberOfUnits)
{
	bool result = true;
	int32 TotalValueForAmmo = 200;
	int32 TotalAmmoRemaining = GetTotalNumberOfBullets();

	int32 RemainingFreeSeats = TotalValueForAmmo - TotalAmmoRemaining;
	if (RemainingFreeSeats >= NumberOfUnits)
	{
		result = false;
	}

	return result;
}

//int32 USEPA_InventoryComponent::GetRemainder_Bullets(EWeaponType WeaponType)
//{
//	int32 NumberOfUnits = 0;
//	int32 CurrentNumberOfUnits = 0;
//	ESizeBox CurrentSizeBox = ESizeBox::None_Box;
//	int32 NumberOfAmmo = 0;
//	int32 NumberOfBullets = 0;
//
//	for (int32 i = 0; i < SlotsAmmo.Num(); i++)
//	{
//		CurrentNumberOfUnits = 0;
//		CurrentSizeBox = ESizeBox::None_Box;
//		NumberOfAmmo = 0;
//		NumberOfBullets = 0;
//
//		NumberOfAmmo = SlotsAmmo[i].NumberOfUnits;
//		CurrentSizeBox = SlotsAmmo[i].SizeBox;
//		USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
//		if (myGameInst)
//		{
//			NumberOfBullets = myGameInst->GetNumberOfUnits(EItemType::Ammo_Type, WeaponType, CurrentSizeBox);
//		}
//
//		CurrentNumberOfUnits = NumberOfAmmo * NumberOfBullets;
//		NumberOfUnits += CurrentNumberOfUnits;
//	}
//
//	return NumberOfUnits;
//}

int32 USEPA_InventoryComponent::GetRemainderFirstAid_BySlotNumber(int32 SlotNumber)
{
	int32 NumberOfUnits = 0;

	NumberOfUnits = SlotsFirstAid[SlotNumber].NumberOfUnits;

	return NumberOfUnits;
}

bool USEPA_InventoryComponent::CheckBeforeReduce(int32 NumberOfUnits1, int32 NumberOfUnits2)
{
	bool result = false;

	if (NumberOfUnits1 >= NumberOfUnits2)
	{
		result = true;
	}

	return result;
}

int32 USEPA_InventoryComponent::GetItemSlotNumber_Ammo(EWeaponType WeaponType)
{
	int32 SlotNumber = 0;

	int32 i = 0;
	bool bIsFind = false;
	if (SlotsAmmo.Num() > 0)
	{
		while (i < SlotsAmmo.Num() && !bIsFind)
		{
			if (SlotsAmmo[i].WeaponType == WeaponType)
			{
				SlotNumber = i;
				bIsFind = true;
			}
			i++;
		}
	}

	return SlotNumber;
}

int32 USEPA_InventoryComponent::GetItemSlotNumber_FirstAid(ESizeBox SizeBox)
{

	int32 SlotNumber = 0;

	int32 i = 0;
	bool bIsFind = false;
	if (SlotsFirstAid.Num() > 0)
	{
		while (i < SlotsFirstAid.Num() && !bIsFind)
		{
			if (SlotsFirstAid[i].SizeBox == SizeBox)
			{
				SlotNumber = i;
				bIsFind = true;
			}
			i++;
		}
	}

	return SlotNumber;
}

void USEPA_InventoryComponent::ReduceByItemBox(EItemType ItemType, EWeaponType WeaponType, ESizeBox SizeBox)
{
	int32 SlotNumber = 0;
	if (ItemType == EItemType::Ammo_Type)
	{
		SlotNumber = GetItemSlotNumber_Ammo(WeaponType);
		int32 RemainderNumberOfUnits = GetRemainderAmmo_BySlotNumber(SlotNumber--);

		int32 NumberOfUnits = 0;
		USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
		if (myGameInst)
		{
			NumberOfUnits = myGameInst->GetNumberOfUnits(ItemType, WeaponType, SizeBox);
		}


		if (CheckBeforeReduce(RemainderNumberOfUnits, NumberOfUnits))
		{
			SlotsAmmo[SlotNumber--].NumberOfUnits -= NumberOfUnits;
		}
	}
	else if (ItemType == EItemType::FirstAid_Type)
	{
		SlotNumber = GetItemSlotNumber_FirstAid(SizeBox);
		//SlotNumber = GetItemSlotNumber(ItemType, EWeaponType::None_Type, SizeBox);

		int32 RemainderNumberOfUnits = GetRemainderFirstAid_BySlotNumber(SlotNumber--);
		if (CheckBeforeReduce(RemainderNumberOfUnits, 1))
		{
			SlotsFirstAid[SlotNumber--].NumberOfUnits--;
		}
	}
}

int32 USEPA_InventoryComponent::GetNumberSlotFA(ESizeBox SizeBox)
{
	int32 result = 0;
	if (SizeBox == ESizeBox::Big_Box)
	{
		result = 1;
	}
	else if (SizeBox == ESizeBox::Medium_Box)
	{
		result = 2;
	}
	else if (SizeBox == ESizeBox::Small_Box)
	{
		result = 3;
	}

	return result;
}

//void USEPA_InventoryComponent::PickUpWeapon(int32 SlotNumber)
//{
//
//}
//
//void USEPA_InventoryComponent::DropWeapon()
//{
//
//}

//MyComment: in this function we don't check remaining ammo, only reduce
void USEPA_InventoryComponent::ReduceAfterReload_Ammo(EWeaponType WeaponType, int32 NumberOfUnits)
{
	int32 SlotNumber = 0;
	SlotNumber = GetItemSlotNumber_Ammo(WeaponType);
	//int32 RemainderNumberOfUnits = GetRemainderAmmo_BySlotNumber(SlotNumber--);

	//if (CheckBeforeReduce(RemainderNumberOfUnits, NumberOfUnits))
	//{
	SlotsAmmo[SlotNumber--].NumberOfUnits -= NumberOfUnits;
	//}
}

EWeaponType USEPA_InventoryComponent::GetWeaponType_BySlotNumber(int32 SlotNumber)
{
	EWeaponType cWeaponType = EWeaponType::None_Type;

	//if (SlotsWeapon.IsValidIndex(SlotNumber))
	//{
	//	cWeaponType = SlotsWeapon[SlotNumber].WeaponType;
	//}

	return cWeaponType;
}

FWeaponInfo USEPA_InventoryComponent::GetWeaponInfo_BySlotNumber(int32 SlotNumber)
{
	EWeaponType New_WeaponType;
	FWeaponInfo New_WeaponInfo;

	New_WeaponType = GetWeaponType_BySlotNumber(SlotNumber);

	USEPA_GameInstance* myGI = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		if (!(myGI->GetWeaponBase_ByWeaponType(New_WeaponType, New_WeaponInfo)))
		{
			UE_LOG(LogTemp, Warning, TEXT("USEPA_InventoryComponent::GetWeaponInfo_BySlotNumber - Weapon not found in table -NULL"));
		}
	}
	
	return New_WeaponInfo;
}

float USEPA_InventoryComponent::GetTotalWeightWeapon()
{
	float resultWeaponWeight = 0.0f;
	float CurrentWeaponWeight = 0.0f;
	//EWeaponType CurrentWeaponType;
	//FWeaponInfo CurrentWeaponInfo;

	//for (int32 i = 0; i < SlotsWeapon.Num(); i++)
	//{
	//	CurrentWeaponWeight = 0.0f;
	//	CurrentWeaponType = SlotsWeapon[i].WeaponType;
	//	USEPA_GameInstance* myGI = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	//	if (myGI)
	//	{
	//		if ((myGI->GetWeaponBase_ByWeaponType(CurrentWeaponType, CurrentWeaponInfo)))
	//		{
	//			CurrentWeaponWeight = CurrentWeaponInfo.WeaponWeight;
	//		}
	//	}
	//	resultWeaponWeight += CurrentWeaponWeight;
	//}
	
	//TODO: we should add place ammo and FirstAid

	return resultWeaponWeight;
}
