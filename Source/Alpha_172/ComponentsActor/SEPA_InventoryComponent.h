// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Alpha_172/GeneralClass/SEPA_Types.h"
#include "SEPA_InventoryComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ALPHA_172_API USEPA_InventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USEPA_InventoryComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	//// Called every frame
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slots")
	//	TArray<FWeaponSlot> SlotsWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slots")
		FWeaponKit WeaponKit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slots")
		TArray<FAmmoSlot> SlotsAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slots")
		TArray<FFirstAidSlot> SlotsFirstAid;


	//MyComment: work with items
	UFUNCTION()
		void PickUpItem(EItemType ItemType, EWeaponType WeaponType, ESizeBox SizeBox);
	UFUNCTION()
		void DropItem(EItemType ItemType, EWeaponType WeaponType, ESizeBox SizeBox);
	UFUNCTION()
		int32 GetItemSlotNumber_Ammo(EWeaponType WeaponType);
	UFUNCTION()
		int32 GetItemSlotNumber_FirstAid(ESizeBox SizeBox);
	//UFUNCTION()
	//	int32 GetItemSlotNumber(EItemType ItemType, EWeaponType WeaponType, ESizeBox SizeBox);

	//MyComment: for drop or use(FirstAid) item, only 1 item
	UFUNCTION()
		int32 GetRemainderAmmo_BySlotNumber(int32 SlotNumber);		//Attention!!! This function for boxes bullets 
	//UFUNCTION()
	//	int32 GetRemainder_Bullets(EWeaponType WeaponType);			//Attention!!! This function for total bullets 
	UFUNCTION()
		int32 GetRemainderAmmo_ByWeaponType(EWeaponType WeaponType);
	UFUNCTION()
		float CalculateNumberOfSeatsPerBullet(EWeaponType WeaponType);
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		int32 GetTotalNumberOfBullets();
	UFUNCTION()
		bool CheckForLackOfPlacesToCollectBullets(int32 NumberOfUnits);

	UFUNCTION()
		int32 GetRemainderFirstAid_BySlotNumber(int32 SlotNumber);
	UFUNCTION()
		bool CheckBeforeReduce(int32 RemainderNumberOfUnits, int32 NumberOfUnits);
	UFUNCTION()
		void ReduceByItemBox(EItemType ItemType, EWeaponType WeaponType, ESizeBox SizeBox);
	UFUNCTION()
		int32 GetNumberSlotFA(ESizeBox SizeBox);

	////MyComment: work with weapon
	//UFUNCTION()
	//	void PickUpWeapon(int32 SlotNumber);
	//UFUNCTION()
	//	void DropWeapon();
		

	UFUNCTION()
		void ReduceAfterReload_Ammo(EWeaponType WeaponType, int32 NumberOfUnits);

	//TODO: marauder, all things of a neutralized character must be available for collection˙

	UFUNCTION()
		EWeaponType GetWeaponType_BySlotNumber(int32 SlotNumber);
	UFUNCTION()
		FWeaponInfo GetWeaponInfo_BySlotNumber(int32 SlotNumber);

	UFUNCTION()
		float GetTotalWeightWeapon();

};
