// Fill out your copyright notice in the Description page of Project Settings.


//#include "ComponentsActor/SEPA_ProtectionComponent.h"
#include "SEPA_ProtectionComponent.h"

// Sets default values for this component's properties
USEPA_ProtectionComponent::USEPA_ProtectionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
}


// Called when the game starts
void USEPA_ProtectionComponent::BeginPlay()
{
	Super::BeginPlay();

	//SetProtectionLevel();
	SetCoefficientOfDamage();
}


// Called every frame
void USEPA_ProtectionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void USEPA_ProtectionComponent::InitializeComponent()
{
	Super::InitializeComponent();

	SetProtectionLevel();
}

float USEPA_ProtectionComponent::GetProtectionPercent()
{
	return Protection;
}

void USEPA_ProtectionComponent::SetProtectionLevel()
{
	ProtectionLevel = 0;
	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		ProtectionLevel = myGameInst->GetProtectionLevel_fromTabl(Protection);
	}
}

int32 USEPA_ProtectionComponent::GetProtectionLevel()
{
	return ProtectionLevel;
}

float USEPA_ProtectionComponent::GetCoefficientOfDamage()
{
	return CoefficientOfDamage;
}

void USEPA_ProtectionComponent::SetCoefficientOfDamage()
{
	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		CoefficientOfDamage = myGameInst->GetCoefficientOfDamage(ProtectionLevel);
	}
}

void USEPA_ProtectionComponent::ChangeProtection_OnDamage(float DamageValue)
{
	if (DamageValue > 0.f)
	{
		int32 ProtectionLevel_befor = ProtectionLevel;
		Protection -= DamageValue;
		int32 reCurrentProtectionLevel = GetProtectionLevel_fromTabl();

		if (Protection <= 0.0f)
		{
			Protection = 0.0f;
			ProtectionLevel = 0;
			OnProtectionChange.Broadcast();
			OnProtectionLevelChange.Broadcast();
		}
		else
		{
			OnProtectionChange.Broadcast();
			if (ProtectionLevel_befor != reCurrentProtectionLevel)
			{
				ProtectionLevel = reCurrentProtectionLevel;
				OnProtectionLevelChange.Broadcast();
				SetCoefficientOfDamage();
			}
		}
	}
}

int32 USEPA_ProtectionComponent::GetProtectionLevel_fromTabl()
{
	int32 reCurrentProtectionLevel = 0;
	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		reCurrentProtectionLevel = myGameInst->GetProtectionLevel_fromTabl(Protection);
	}

	return reCurrentProtectionLevel;
}