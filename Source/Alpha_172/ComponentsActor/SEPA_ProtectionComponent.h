// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Alpha_172/GameClass/SEPA_GameInstance.h"
#include "SEPA_ProtectionComponent.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnProtectionChange, float, CurrentProtection);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnProtectionLevelChange);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnProtectionChange);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ALPHA_172_API USEPA_ProtectionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USEPA_ProtectionComponent();

	UPROPERTY(BlueprintAssignable, Category = "Protection")
		FOnProtectionLevelChange OnProtectionLevelChange;
	UPROPERTY(BlueprintAssignable, Category = "Protection")
		FOnProtectionChange OnProtectionChange;

	
	virtual void InitializeComponent() override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY()
		float Protection = 60.f;
	UPROPERTY()
		int32 ProtectionLevel = 0;
	UPROPERTY()
		float CoefficientOfDamage = 0.f;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Protection")
		float GetProtectionPercent();
	UFUNCTION(BlueprintCallable, Category = "Protection")
		int32 GetProtectionLevel();
	UFUNCTION(BlueprintCallable, Category = "Protection")
		void SetProtectionLevel();
	UFUNCTION(BlueprintCallable, Category = "Protection")
		float GetCoefficientOfDamage();
	UFUNCTION(BlueprintCallable, Category = "Protection")
		void SetCoefficientOfDamage();

	UFUNCTION(BlueprintCallable, Category = "Protection")
		void ChangeProtection_OnDamage(float DamageValue);
	UFUNCTION(BlueprintCallable, Category = "Protection")
		int32 GetProtectionLevel_fromTabl();
};
