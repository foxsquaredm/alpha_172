// Fill out your copyright notice in the Description page of Project Settings.


#include "SEPA_WeaponComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
USEPA_WeaponComponent::USEPA_WeaponComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


//// Called when the game starts
//void USEPA_WeaponComponent::BeginPlay()
//{
//	Super::BeginPlay();
//
//	// ...
//	
//}
//
//
//// Called every frame
//void USEPA_WeaponComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
//{
//	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//
//	FireTick(DeltaTime);
//	ReloadTick(DeltaTime);
//	DispersionTick(DeltaTime);
//
//}
//
//void USEPA_WeaponComponent::FireTick(float DeltaTime)
//{
//	//if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
//	//{
//	if (FireTimer < 0.f)
//	{
//		//if (!WeaponReloading)
//		Fire();
//	}
//	else
//	{
//		FireTimer -= DeltaTime;
//	}
//	//}
//	////else
//	////{
//	////	if (!WeaponReloading && CheckCanWeaponReload())
//	////	{
//	////		InitReload();
//	////	}
//	////}
//}
//
//void USEPA_WeaponComponent::ReloadTick(float DeltaTime)
//{
//	//if (WeaponReloading)
//	//{
//	if (ReloadTimer < 0.0f)
//	{
//		//FinishReload();
//	}
//	else
//	{
//		ReloadTimer -= DeltaTime;
//	}
//	//}
//}
//
//void USEPA_WeaponComponent::DispersionTick(float DeltaTime)
//{
//	//if (!WeaponReloading)
//	//{
//	//	if (!WeaponFiring)
//	//	{
//	//		if (ShouldReduceDispersion)
//	//			CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
//	//		else
//	//			CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
//	//	}
//
//	//	if (CurrentDispersion < CurrentDispersionMin)
//	//	{
//
//	//		CurrentDispersion = CurrentDispersionMin;
//
//	//	}
//	//	else
//	//	{
//	//		if (CurrentDispersion > CurrentDispersionMax)
//	//		{
//	//			CurrentDispersion = CurrentDispersionMax;
//	//		}
//	//	}
//	//}
//	//if (ShowDebug)
//	//	UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
//}

//void USEPA_WeaponComponent::Fire()
//{
//	UAnimMontage* AnimToPlay = nullptr;
//	AnimToPlay = WeaponInfo.AnimCharacter_Fire;
//	//AnimWeaponStart_Multicast(AnimToPlay);
//
//	FireTimer = WeaponInfo.RateOfFire;
//	//WeaponInfo.Round --;
//	//ChangeDispersionByShot();
//	//OnWeaponFireStart.Broadcast(AnimToPlay);
//
//	//FXWeaponFire_Multicast(WeaponInfo.EffectFireWeapon, WeaponInfo.SoundFireWeapon);
//
//
//	int8 NumberProjectile = GetNumberProjectileByShot();
//
//	////if (ShootLocation)
//	////{
//	//	FVector SpawnLocation = ShootLocation->GetComponentLocation();
//	//	FRotator SpawnRotation = ShootLocation->GetComponentRotation();
//	//	FProjectileInfo ProjectileInfo;
//	////	ProjectileInfo = GetProjectile();
//
//	//	FVector EndLocation;
//	//	for (int8 i = 0; i < NumberProjectile; i++)		//if Shotgun, when i > 1
//	//	{
//	//		EndLocation = GetFireEndLocation();
//
//	//		FVector Dir = EndLocation - SpawnLocation;
//	//		Dir.Normalize();
//	//		FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
//	//		SpawnRotation = myMatrix.Rotator();
//
//	////		if (ProjectileInfo.Projectile)
//	////		{
//	////			//Projectile Init ballistic fire
//
//	//			FActorSpawnParameters SpawnParams;
//	//			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
//	//			SpawnParams.Owner = GetOwner();
//	//			//SpawnParams.Instigator = GetInstigator();
//
//	//			//AProjectile* myProjectile = Cast<AProjectile>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
//	////			if (myProjectile)
//	////			{
//	////				myProjectile->InitProjectile(WeaponInfo.ProjectileSetting);
//
//	////				//drop shell bullets
//	////				AActor* curShellBullets = GetWorld()->SpawnActor<AActor>(WeaponInfo.ShellBullets, GetActorLocation(), FRotator::ZeroRotator);
//	////				if (curShellBullets)
//	////				{
//	////					UPrimitiveComponent* PrimitivElement = Cast<UPrimitiveComponent>(curShellBullets->GetComponentByClass(UPrimitiveComponent::StaticClass()));
//	////					if (PrimitivElement)
//	////					{
//	////						PrimitivElement->AddImpulse(GetActorRightVector() * 2.f);
//	////					}
//	////				}
//
//	////				UE_LOG(LogTemp, Warning, TEXT("Droped shell bullets.================"));
//	////			}
//	////		}
//	////		else
//	////		{
//	////			//Multicast trace FX
//	////			//ToDo Projectile null Init trace fire			
//
//	////			//GetWorld()->LineTraceSingleByChannel()
//	//			FHitResult Hit;
//	//			TArray<AActor*> Actors;
//
//	//			EDrawDebugTrace::Type DebugTrace;
//	//			if (ShowDebug)
//	//			{
//	//				DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponInfo.ShotRange, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
//	//				DebugTrace = EDrawDebugTrace::ForDuration;
//	//			}
//	//			else
//	//				DebugTrace = EDrawDebugTrace::None;
//
//	//			UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponInfo.ShotRange,
//	//				ETraceTypeQuery::TraceTypeQuery4, false, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);
//
//	//			if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
//	//			{
//	//				EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
//
//	////				if (WeaponInfo.ProjectileSetting.HitDecals.Contains(mySurfacetype))
//	////				{
//	//					UMaterialInterface* myMaterial = WeaponInfo.ProjectileSetting.HitDecals[mySurfacetype];
//
//	//					if (myMaterial && Hit.GetComponent())
//	//					{
//	//						UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
//	//					}
//	////				}
//	////				if (WeaponInfo.ProjectileSetting.HitFXs.Contains(mySurfacetype))
//	////				{
//	////					UParticleSystem* myParticle = WeaponInfo.ProjectileSetting.HitFXs[mySurfacetype];
//	////					if (myParticle)
//	////					{
//	////						UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
//	////					}
//	////				}
//
//	////				if (WeaponInfo.ProjectileSetting.HitSound)
//	////				{
//	////					UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponInfo.ProjectileSetting.HitSound, Hit.ImpactPoint);
//	////				}
//
//	////				UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileInfo.Effect, mySurfacetype);
//
//	////				UGameplayStatics::ApplyPointDamage(Hit.GetActor(), ProjectileInfo.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
//	//			}
//	////		}
//	//	}
//	////}
//
//	////if (GetWeaponRound() <= 0 && !WeaponReloading)
//	////{
//	////	//Init reload
//	////	if (CheckCanWeaponReload())
//	////	{
//	////		InitReload();
//	////	}
//	////}
//
//}
//
//void USEPA_WeaponComponent::Reload_Start()
//{
//	FVector SpawnLocation = FVector(0);
//	//UGameplayStatics::SpawnSoundAtLocation(GetWorld(), CurrentWeapon->WeaponInfo.SoundReloadWeapon, SpawnLocation);
//
//	//TODO: drop magazine !!!
//
//
//	//WeaponReloadStart_BP(Anim);
//}
//
//void USEPA_WeaponComponent::Reload_Finish()
//{
//	//if (InventoryComponent && CurrentWeapon)
//	//{
//	//	InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponInfo.WeaponType, AmmoTake);
//	//	InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
//	//}
//
//	//WeaponReloadEnd_BP(bIsSuccess);
//}
//
//float USEPA_WeaponComponent::GetTimeToReload()
//{
//	float TimeToReload = 0.0f;
//	int32 NumberOfBulletsTotal = 0;
//	int32 NumberOfBulletsRemaining = 0;
//	float ReloadTime = 0.0f;
//
//	if (NumberOfBulletsRemaining == 0.0f)
//	{
//		TimeToReload = ReloadTime;
//	}
//	else
//	{
//		TimeToReload = ReloadTime * (1 - (NumberOfBulletsTotal / (100 * NumberOfBulletsRemaining)));
//	}
//
//	return TimeToReload;
//}
//
//FVector USEPA_WeaponComponent::GetFireEndLocation() const
//{
//	bool bShootDirection = false;
//	FVector EndLocation = FVector(0.f);
//
//	//FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
//	////UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());
//
//	//if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
//	//{
//	//	EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
//	//	if (ShowDebug)
//	//		DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponInfo.ShotRange, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
//	//}
//	//else
//	//{
//	//	EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
//	//	if (ShowDebug)
//	//		DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponInfo.ShotRange, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
//	//}
//
//
//	//if (ShowDebug)
//	//{
//	//	//direction weapon look
//	//	DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
//	//	//direction projectile must fly
//	//	DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
//	//	//Direction Projectile Current fly
//	//	DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
//
//	//	//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
//	//}
//
//
//	return EndLocation;
//}
//
//int8 USEPA_WeaponComponent::GetNumberProjectileByShot() const
//{
//	return WeaponInfo.NumberProjectileByShot;
//}
//
