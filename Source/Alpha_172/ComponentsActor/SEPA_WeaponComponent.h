// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Alpha_172/GeneralClass/SEPA_Types.h"
#include "Alpha_172/WeaponClass/SEPA_WeaponBase.h"
#include "Alpha_172/WeaponClass/ProjectileClass/SEPA_ProjectileBase.h"
#include "SEPA_WeaponComponent.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFire,			UAnimMontage*, AnimFire);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart,	UAnimMontage*, AnimReload);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadFinish, bool, bIsSuccess, int32, RemainingAmmo);		//AmmoSafe

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ALPHA_172_API USEPA_WeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USEPA_WeaponComponent();

	//FOnWeaponFire			OnWeaponFire;
	//FOnWeaponReloadStart	OnWeaponReloadStart;
	//FOnWeaponReloadFinish	OnWeaponReloadFinish;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	//	class USceneComponent* SceneComponent = nullptr;
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	//	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	//	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	//	class UArrowComponent* ShootLocation = nullptr;

//	UPROPERTY()
//		FWeaponInfo WeaponInfo;			//WeaponSetting
//	UPROPERTY()
//		FProjectileInfo ProjectileInfo;
//
//protected:
//	// Called when the game starts
//	virtual void BeginPlay() override;
//
//public:	
//	// Called every frame
//	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
//
//
//	UPROPERTY()
//		ASEPA_WeaponBase* CurrentWeapon;
//
//	//UFUNCTION()
//	//	ASEPA_WeaponBase* GetCurrentWeapon();
//
//	void FireTick(float DeltaTime);
//	void ReloadTick(float DeltaTime);
//	void DispersionTick(float DeltaTime);
//
//	void Fire();
//	void Reload_Start();
//	void Reload_Finish();
//	float GetTimeToReload();
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
//		bool ShowDebug = false;
//
//	FVector GetFireEndLocation() const;
//	int8 GetNumberProjectileByShot() const;
//
//	//Timers
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
//		float FireTimer = 0.0f;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer")
//		float ReloadTimer = 0.0f;
//	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RunTimer Debug")	//Remove !!! Debug
//	//	float ReloadTime = 0.0f;
//
//	//change current weapon	
//	UFUNCTION()
//		void SwitchWeapon(bool NextWeapon);

};
