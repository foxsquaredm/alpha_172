// Fill out your copyright notice in the Description page of Project Settings.


//#include "ComponentsActor/SEPA_StaminaComponent.h"
#include "SEPA_StaminaComponent.h"

// Sets default values for this component's properties
USEPA_StaminaComponent::USEPA_StaminaComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USEPA_StaminaComponent::BeginPlay()
{
	Super::BeginPlay();

	SetTimeESD();
	//SetCoefficientOfChangeStamina(EMovementState::StandStill_State, EPositionState::Stand_State, true);
}


void USEPA_StaminaComponent::SetEnableStaminaRecovery(bool Value)
{
	Enable_StaminaRecovery = Value;
}

// Called every frame
void USEPA_StaminaComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (CharacterMakeMoreEffort)
	{
		Stamina -= CoefficientOfChangeStamina;
		OnStaminaChange.Broadcast();
	}
	else
	{
		if (Stamina < 100.f && !Enable_StaminaRecovery)
		{
			GetWorld()->GetTimerManager().SetTimer(Timer_StaminaRecovery, this, &USEPA_StaminaComponent::Execution_StaminaRecovery, rateTimeESD, true);
			Enable_StaminaRecovery = true;
		}
	}
}

float USEPA_StaminaComponent::GetStaminaPercent()
{
	return round(Stamina);
}

void USEPA_StaminaComponent::SetCoefficientOfChangeStamina(EMovementState MovementState, EPositionState PositionState, bool isRecovery)
{
	CoefficientOfChangeStamina = 0.f;
	USEPA_GameInstance* myGameInst = Cast<USEPA_GameInstance>(GetWorld()->GetGameInstance());
	if (myGameInst)
	{
		CoefficientOfChangeStamina = myGameInst->GetCoefficientOfChangeStamina(MovementState, PositionState, isRecovery);
	}
}

void USEPA_StaminaComponent::SetCharacterMakeMoreEffort(bool Value)
{
	CharacterMakeMoreEffort = Value;
}

void USEPA_StaminaComponent::SetTimeESD()
{
	rateTimeESD = Time_TotalRecovery / 50;
}

void USEPA_StaminaComponent::Execution_StaminaRecovery()
{
	Stamina = Stamina + CoefficientOfChangeStamina;
	if (Stamina >= 100.f)
	{
		Stamina = 100.f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(Timer_StaminaRecovery);
			Enable_StaminaRecovery = false;
		}
	}
	else
	{
		//Stamina = Stamina + CoefficientOfChangeStamina;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().SetTimer(Timer_StaminaRecovery, this, &USEPA_StaminaComponent::Execution_StaminaRecovery, rateTimeESD, true);
		}
	}
	OnStaminaChange.Broadcast();
}


//void USEPA_StaminaComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
//{
//	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
//
//	//DOREPLIFETIME(USEPA_HealthComponent, Health);
//
//}